// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';

class MainToolbar extends StatelessWidget {
  final MainToolbarModel model;

  const MainToolbar(this.model, {super.key});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: AppColors.light,
      leading: Visibility(
        visible: model.leftIcon != null,
        child: IconButton(
          icon: model.leftIcon!,
          onPressed: () {
            model.onLeftIconClicked?.call();
          },
          color: AppColors.primary,
        ),
      ),
      centerTitle: true,
      elevation: 0.5,
      actions: [
        Visibility(
          visible: model.rightIcon != null,
          child: IconButton(
            onPressed: () {
              model.onRightIconClicked?.call();
            },
            color: AppColors.primary,
            icon: model.rightIcon!,
          ),
        )
      ],
      title: Text(
        model.title,
        style: model.titleStyle,
      ),
    );
  }
}

class MainToolbarModel {
  MainToolbarModel(
      {required this.title,
      required this.titleStyle,
      this.onLeftIconClicked,
      this.leftIcon,
      this.onRightIconClicked,
      this.rightIcon});

  final Function? onLeftIconClicked;
  final Icon? leftIcon;
  final Function? onRightIconClicked;
  final Icon? rightIcon;
  final String title;
  final TextStyle titleStyle;
}
