// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';

class PlainPopUp extends StatelessWidget {
  const PlainPopUp({super.key, required this.model});

  final PlainPopUpModel model;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      maintainBottomViewPadding: true,
      child: Container(
        width: double.infinity,
        color: AppColors.light,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                model.title,
                style: AppStyles.anekLatinBold,
              ),
              const SizedBox(height: 8),
              Text(
                model.subTitle,
                style: AppStyles.anekLatinRegular.copyWith(
                  fontSize: 12,
                ),
              ),
              const SizedBox(height: 16),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  style: AppStyles.primaryButton,
                  onPressed: () {
                    model.onClicked();
                  },
                  child: const Text("Okay"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class PlainPopUpModel {
  final String title;
  final String subTitle;
  final Function() onClicked;
  PlainPopUpModel({
    this.title = '',
    this.subTitle = '',
    required this.onClicked,
  });
}
