// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';

import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';

class CategoryEventItem extends StatefulWidget {
  final CategoryModel model;
  final Function(int id) onCategoryItemClicked;
  const CategoryEventItem({
    Key? key,
    required this.model,
    required this.onCategoryItemClicked,
  }) : super(key: key);

  @override
  State<CategoryEventItem> createState() => _CategoryEventItemState();
}

class _CategoryEventItemState extends State<CategoryEventItem> {
  @override
  Widget build(BuildContext context) {
    var model = widget.model;
    return Container(
      decoration: BoxDecoration(
        boxShadow: (model.isSelected)
            ? [
                BoxShadow(
                  offset: const Offset(0, 0.4),
                  blurRadius: 5,
                  color: Colors.black.withOpacity(0.3),
                ),
              ]
            : [
                BoxShadow(
                  offset: const Offset(0, 0.25),
                  blurRadius: 5,
                  color: Colors.black.withOpacity(0.1),
                ),
              ],
        color: (model.isSelected)
            ? AppColors.cardBackgroundActive
            : AppColors.cardBackground,
        border: Border.all(
          width: (model.isSelected) ? 2 : 1,
          color: (model.isSelected)
              ? AppColors.cardBorderActive
              : AppColors.cardBorder,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(8.0),
        ),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(8),
          onTap: () {
            widget.onCategoryItemClicked(model.id);
          },
          child: Padding(
            padding: const EdgeInsets.only(
              top: 4,
              bottom: 4,
              left: 8,
              right: 8,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Icon(
                  _getCategoryIcon(),
                  color: AppColors.primary,
                  size: 28,
                ),
                Text(
                  widget.model.name,
                  style: AppStyles.anekLatinSemiBold
                      .copyWith(color: AppColors.primary, fontSize: 12),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  IconData _getCategoryIcon() {
    return (widget.model.id == "0")
        ? Icons.sports_football
        : Icons.sports_baseball;
  }
}
