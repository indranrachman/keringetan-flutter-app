// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:keringetan_consumer_flutter_app/core/constants/event_status.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';

class EventStatusBadge extends StatelessWidget {
  const EventStatusBadge({
    Key? key,
    required this.model,
  }) : super(key: key);

  final EventModel model;

  @override
  Widget build(BuildContext context) {
    var statusName = "";
    var textColor = AppColors.textAccent;
    var backgroundColor = AppColors.secondary;

    switch (EventStatusUtils.fromString(model.state)) {
      case EventStatus.byYou:
        statusName = "It's you";
        textColor = AppColors.pillOrangeText;
        backgroundColor = AppColors.pillOrangeBackground;
        break;
      case EventStatus.open:
        statusName = "Nearby";
        textColor = AppColors.pillBlueText;
        backgroundColor = AppColors.pillBlueBackground;
        break;
      case EventStatus.joined:
        statusName = "Joined";
        textColor = AppColors.pillGreenText;
        backgroundColor = AppColors.pillGreenBackground;
        break;
      case EventStatus.unknown:
        statusName = "Unknown status";
        textColor = AppColors.pillMutedText;
        backgroundColor = AppColors.pillMutedBackground;
        break;
    }

    return Container(
      width: 64,
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: const BorderRadius.all(
          Radius.circular(8),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(
          top: 4,
          bottom: 4,
          left: 8,
          right: 8,
        ),
        child: Text(
          textAlign: TextAlign.center,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          statusName,
          style: AppStyles.anekLatinBold.copyWith(
            fontSize: 12,
            color: textColor,
          ),
        ),
      ),
    );
  }
}
