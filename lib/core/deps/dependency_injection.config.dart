// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i12;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../../features/create_event/data/create_event_repository.dart' as _i17;
import '../../features/create_event/presentation/bloc/create_event_bloc.dart'
    as _i23;
import '../../features/edit_event/data/edit_event_repository.dart' as _i18;
import '../../features/edit_event/presentation/bloc/edit_event_bloc.dart'
    as _i24;
import '../../features/edit_event/presentation/mapper/edit_event_mapper.dart'
    as _i3;
import '../../features/edit_profile/presentation/bloc/edit_profile_bloc.dart'
    as _i4;
import '../../features/explore/data/explore_repository.dart' as _i14;
import '../../features/explore/presentation/bloc/explore_bloc.dart' as _i19;
import '../../features/explore/presentation/bloc/explore_state.dart' as _i6;
import '../../features/explore/presentation/mapper/explore_mapper.dart' as _i5;
import '../../features/explore/presentation/model/explore_model.dart' as _i7;
import '../../features/location_picker/presentation/bloc/location_picker_bloc.dart'
    as _i20;
import '../../features/login/data/login_repository.dart' as _i21;
import '../../features/login/presentation/bloc/login_bloc.dart' as _i25;
import '../../features/profile/data/profile_repository.dart' as _i22;
import '../../features/profile/presentation/bloc/profile_bloc.dart' as _i26;
import '../apis/event/event_service.dart' as _i13;
import '../apis/google/google_sign_in_provider.dart' as _i8;
import '../apis/google/place/place_service.dart' as _i15;
import '../apis/user/user_mapper.dart' as _i11;
import '../apis/user/user_service.dart' as _i16;
import '../utils/pref_utils.dart' as _i9;
import '../utils/user_location_service.dart' as _i10;
import 'modules/app_module.dart' as _i27;

// ignore_for_file: unnecessary_lambdas
// ignore_for_file: lines_longer_than_80_chars
// initializes the registration of main-scope dependencies inside of GetIt
Future<_i1.GetIt> $initGetIt(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) async {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final appModule = _$AppModule();
  gh.factory<_i3.EditEventMapper>(() => _i3.EditEventMapperImpl());
  gh.factory<_i4.EditProfileBloc>(() => _i4.EditProfileBloc());
  gh.factory<_i5.ExploreMapper>(() => _i5.ExploreMapperImpl());
  gh.factory<_i6.ExploreState>(() => _i6.ExploreState(
        error: gh<String>(),
        showLoading: gh<bool>(),
        pullToRefresh: gh<bool>(),
        items: gh<List<_i7.EventModel>>(),
      ));
  gh.factory<_i8.GoogleSignInProvider>(() => _i8.GoogleSignInProviderImpl());
  await gh.factoryAsync<_i9.PrefUtils>(
    () => appModule.getPrefsUtils(),
    preResolve: true,
  );
  gh.factory<_i10.UserLocationService>(
      () => _i10.UserLocationServiceImpl(prefUtils: gh<_i9.PrefUtils>()));
  gh.factory<_i11.UserMapper>(() => _i11.UserMapperImpl());
  await gh.factoryAsync<_i12.Dio>(
    () => appModule.getDio(gh<_i9.PrefUtils>()),
    preResolve: true,
  );
  gh.factory<_i13.EventService>(() => _i13.EventServiceImpl(
        dio: gh<_i12.Dio>(),
        prefUtils: gh<_i9.PrefUtils>(),
      ));
  gh.factory<_i14.ExploreRepository>(
      () => _i14.ExploreRespositoryImpl(eventService: gh<_i13.EventService>()));
  gh.factory<_i15.PlaceService>(
      () => _i15.PlaceServiceImpl(prefUtils: gh<_i9.PrefUtils>()));
  gh.factory<_i16.UserService>(() => _i16.UserServiceImpl(dio: gh<_i12.Dio>()));
  gh.factory<_i17.CreateEventRepository>(
      () => _i17.CreateEventRepositoryImpl(service: gh<_i13.EventService>()));
  gh.factory<_i18.EditEventRepository>(
      () => _i18.EditEventRepositoryImpl(service: gh<_i13.EventService>()));
  gh.factory<_i19.ExploreBloc>(() => _i19.ExploreBloc(
        gh<_i14.ExploreRepository>(),
        gh<_i5.ExploreMapper>(),
        gh<_i10.UserLocationService>(),
        gh<_i9.PrefUtils>(),
      ));
  gh.factory<_i20.LocationPickerBloc>(
      () => _i20.LocationPickerBloc(gh<_i15.PlaceService>()));
  gh.factory<_i21.LoginRepository>(() => _i21.LoginRepositoryImpl(
        googleSignInProvider: gh<_i8.GoogleSignInProvider>(),
        userService: gh<_i16.UserService>(),
        prefUtils: gh<_i9.PrefUtils>(),
      ));
  gh.factory<_i22.ProfileRepository>(() => _i22.ProfileRepositoryImpl(
        signInProvider: gh<_i8.GoogleSignInProvider>(),
        userService: gh<_i16.UserService>(),
        prefUtils: gh<_i9.PrefUtils>(),
      ));
  gh.factory<_i23.CreateEventBloc>(() => _i23.CreateEventBloc(
        gh<_i17.CreateEventRepository>(),
        gh<_i9.PrefUtils>(),
      ));
  gh.factory<_i24.EditEventBloc>(() => _i24.EditEventBloc(
        gh<_i18.EditEventRepository>(),
        gh<_i3.EditEventMapper>(),
      ));
  gh.factory<_i25.LoginBloc>(() => _i25.LoginBloc(gh<_i21.LoginRepository>()));
  gh.factory<_i26.ProfileBloc>(
      () => _i26.ProfileBloc(gh<_i22.ProfileRepository>()));
  return getIt;
}

class _$AppModule extends _i27.AppModule {}
