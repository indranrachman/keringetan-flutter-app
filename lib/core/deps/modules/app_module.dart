import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/pref_utils.dart';

@module
abstract class AppModule {
  @preResolve
  Future<PrefUtils> getPrefsUtils() async {
    var prefs = PrefUtils();
    await prefs.init();
    return prefs;
  }

  @preResolve
  Future<Dio> getDio(PrefUtils prefUtils) async {
    var dio = Dio();
    var baseUrl =
        kReleaseMode ? 'https://api.keringetan.com' : 'http://127.0.0.1:8080';
    final options = BaseOptions(
      headers: {
        'X-App-Key': 'TEST_APP_KEY',
      },
      baseUrl: baseUrl,
      connectTimeout: const Duration(seconds: 5),
      receiveTimeout: const Duration(seconds: 3),
    );
    dio.options = options;
    dio.interceptors.add(LogInterceptor());
    return dio;
  }
}
