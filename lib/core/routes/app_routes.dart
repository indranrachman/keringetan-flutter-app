import 'package:flutter/material.dart';
import 'package:keringetan_consumer_flutter_app/features/create_event/ui/create_event_page.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/ui/explore_page.dart';
import 'package:keringetan_consumer_flutter_app/features/my_activities/ui/my_activities_page.dart';
import 'package:keringetan_consumer_flutter_app/features/profile/ui/profile_page.dart';

class AppRoutes {
  static const String explorePage = '/explore_page';
  static const String myActivitiesPage = '/my_activities_page';
  static const String profilePage = '/profile_page';
  static const String createActivity = '/create_activity';

  static Map<String, WidgetBuilder> get routes => {
        explorePage: ExplorePage.builder,
        myActivitiesPage: MyActivitiesPage.builder,
        profilePage: ProfilePage.builder,
        createActivity: CreateEventPage.builder,
      };
}
