// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:firebase_auth/firebase_auth.dart';

class FirebaseSignUpProvider {
  static Future<SignUpResult> signUpWithEmailAndPassword(
    String email,
    String password,
  ) async {
    try {
      final credential =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      return SignUpResult(user: credential.user, error: null);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        return SignUpResult(
          user: null,
          error: "The password provided is too weak.",
        );
      } else if (e.code == 'email-already-in-use') {
        return SignUpResult(
          user: null,
          error: "The account already exists for that email.",
        );
      } else if (e.code == 'invalid-email') {
        return SignUpResult(
          user: null,
          error: "Please use a valid email address",
        );
      }
      return SignUpResult(
        user: null,
        error: e.code,
      );
    } catch (e) {
      return SignUpResult(
        user: null,
        error: null,
      );
    }
  }
}

class SignUpResult {
  final User? user;
  final String? error;
  SignUpResult({
    this.user,
    required this.error,
  });
}
