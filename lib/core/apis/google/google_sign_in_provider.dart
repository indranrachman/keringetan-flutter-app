// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:injectable/injectable.dart';

class GoogleSignInProviderResponse {
  final User? user;
  final String? errorMessage;
  GoogleSignInProviderResponse({
    required this.user,
    required this.errorMessage,
  });
}

abstract class GoogleSignInProvider {
  Future<GoogleSignInProviderResponse> signIn();
  Future<bool> signOut();
}

@Injectable(as: GoogleSignInProvider)
class GoogleSignInProviderImpl implements GoogleSignInProvider {
  @override
  Future<GoogleSignInProviderResponse> signIn() async {
    FirebaseAuth firebaseAuth = FirebaseAuth.instance;
    User? user;

    var googleSignIn = GoogleSignIn();

    var account = await googleSignIn.signIn();

    if (account != null) {
      var auth = await account.authentication;

      var credential = GoogleAuthProvider.credential(
        accessToken: auth.accessToken,
        idToken: auth.idToken,
      );

      try {
        var credentials = await firebaseAuth.signInWithCredential(credential);
        user = credentials.user;
        if (user == null) {
          return GoogleSignInProviderResponse(
            user: null,
            errorMessage: null,
          );
        } else {
          return GoogleSignInProviderResponse(
            user: user,
            errorMessage: null,
          );
        }
      } on FirebaseAuthException catch (e) {
        return _handleFirebaseError(e);
      } catch (e) {
        return GoogleSignInProviderResponse(
          user: null,
          errorMessage: null,
        );
      }
    }
    return GoogleSignInProviderResponse(
      user: null,
      errorMessage: null,
    );
  }

  GoogleSignInProviderResponse _handleFirebaseError(FirebaseAuthException e) {
    if (e.code == 'account-exists-with-different-credential') {
      return GoogleSignInProviderResponse(
        user: null,
        errorMessage: 'Account already exists with a different credential',
      );
    } else if (e.code == 'invalid-credential') {
      return GoogleSignInProviderResponse(
        user: null,
        errorMessage: 'Error occurred using Google Sign In. Try again.',
      );
    }
    return GoogleSignInProviderResponse(
      user: null,
      errorMessage: e.code,
    );
  }

  @override
  Future<bool> signOut() async {
    try {
      await FirebaseAuth.instance.signOut();
      return true;
    } catch (e) {
      return false;
    }
  }
}
