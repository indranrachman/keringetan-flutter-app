// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:flutter/foundation.dart';

class LocationPickerModel {
  final List<PlaceSuggestionModel> locations;
  LocationPickerModel({
    this.locations = const [],
  });

  LocationPickerModel copyWith({
    List<PlaceSuggestionModel>? locations,
  }) {
    return LocationPickerModel(
      locations: locations ?? this.locations,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'locations': locations.map((x) => x.toMap()).toList(),
    };
  }

  factory LocationPickerModel.fromMap(Map<String, dynamic> map) {
    return LocationPickerModel(
      locations: List<PlaceSuggestionModel>.from(
        (map['locations'] as List).map<PlaceSuggestionModel>(
          (x) => PlaceSuggestionModel.fromMap(x as Map<String, dynamic>),
        ),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory LocationPickerModel.fromJson(String source) =>
      LocationPickerModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'LocationPickerModel(locations: $locations)';

  @override
  bool operator ==(covariant LocationPickerModel other) {
    if (identical(this, other)) return true;

    return listEquals(other.locations, locations);
  }

  @override
  int get hashCode => locations.hashCode;
}

class PlaceSuggestionModel {
  final String placeId;
  final String name;
  final String description;
  final double latitude;
  final double longitude;
  bool isLoading;
  PlaceSuggestionModel({
    this.placeId = '',
    this.name = '',
    this.description = '',
    this.latitude = 0.0,
    this.longitude = 0.0,
    this.isLoading = false,
  });

  PlaceSuggestionModel copyWith({
    String? placeId,
    String? name,
    String? description,
    double? latitude,
    double? longitude,
    bool? isLoading,
  }) {
    return PlaceSuggestionModel(
      placeId: placeId ?? this.placeId,
      name: name ?? this.name,
      description: description ?? this.description,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      isLoading: isLoading ?? this.isLoading,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'placeId': placeId,
      'name': name,
      'description': description,
      'latitude': latitude,
      'longitude': longitude,
      'isLoading': isLoading,
    };
  }

  factory PlaceSuggestionModel.fromMap(Map<String, dynamic> map) {
    return PlaceSuggestionModel(
      placeId: map['placeId'] as String,
      name: map['name'] as String,
      description: map['description'] as String,
      latitude: map['latitude'] as double,
      longitude: map['longitude'] as double,
      isLoading: map['isLoading'] as bool,
    );
  }

  String toJson() => json.encode(toMap());

  factory PlaceSuggestionModel.fromJson(String source) =>
      PlaceSuggestionModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'PlaceSuggestionModel(placeId: $placeId, name: $name, description: $description, latitude: $latitude, longitude: $longitude, isLoading: $isLoading)';
  }

  @override
  bool operator ==(covariant PlaceSuggestionModel other) {
    if (identical(this, other)) return true;

    return other.placeId == placeId &&
        other.name == name &&
        other.description == description &&
        other.latitude == latitude &&
        other.longitude == longitude &&
        other.isLoading == isLoading;
  }

  @override
  int get hashCode {
    return placeId.hashCode ^
        name.hashCode ^
        description.hashCode ^
        latitude.hashCode ^
        longitude.hashCode ^
        isLoading.hashCode;
  }
}
