// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/google/place/model/place_suggestion_model.dart';
import 'package:keringetan_consumer_flutter_app/core/extensions/string_ext.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/pref_utils.dart';
import 'package:uuid/uuid.dart';

abstract class PlaceService {
  Future<List<PlaceSuggestionModel>> fetchPlaces(
    String input,
    String lang,
  );

  Future<PlaceSuggestionModel> fetchPlaceDetails(
    PlaceSuggestionModel model,
  );
  List<PlaceSuggestionModel> fetchRecentSearches();
}

@Injectable(as: PlaceService)
class PlaceServiceImpl implements PlaceService {
  static final String _iosKey = dotenv.env['GMAP_PLACES_IOS_API_KEY'].orEmpty();
  static final String _androidKey =
      dotenv.env['GMAP_PLACES_ANDROID_API_KEY'].orEmpty();
  static const radius = 30000;

  final Dio _dio = Dio()..interceptors.add(LogInterceptor());
  final String apiKey = Platform.isAndroid ? _androidKey : _iosKey;
  final String sessionToken = const Uuid().v4();
  final PrefUtils prefUtils;

  PlaceServiceImpl({
    required this.prefUtils,
  });

  @override
  Future<List<PlaceSuggestionModel>> fetchPlaces(
    String input,
    String countryCode,
  ) async {
    final lat = prefUtils.getUserLat();
    final lng = prefUtils.getUserLng();
    final request =
        'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$input&language=$countryCode&components=country:$countryCode&key=$apiKey&sessiontoken=$sessionToken&location=$lat,$lng&radius=$radius';
    final response = await _dio.get(request);

    if (response.statusCode == 200) {
      final result = response.data;
      if (result['status'] == 'OK') {
        // compose suggestions in a list
        var locations = result['predictions']
            .map<PlaceSuggestionModel>(
              (place) => PlaceSuggestionModel(
                placeId: place['place_id'],
                name: place['structured_formatting']['main_text'],
                description: place['structured_formatting']['secondary_text'],
              ),
            )
            .toList();
        var model = LocationPickerModel(locations: locations);
        prefUtils.setRecentLocationSearch(model.toJson());
        return locations;
      } else if (result['status'] == 'ZERO_RESULTS') {
        return [];
      }
      throw Exception(result['error_message']);
    } else {
      throw Exception('Failed to fetch suggestion');
    }
  }

  @override
  List<PlaceSuggestionModel> fetchRecentSearches() {
    return LocationPickerModel.fromJson(prefUtils.getRecentLocationSearch())
        .locations;
  }

  @override
  Future<PlaceSuggestionModel> fetchPlaceDetails(
    PlaceSuggestionModel model,
  ) async {
    final request =
        'https://maps.googleapis.com/maps/api/place/details/json?place_id=${model.placeId}&key=$apiKey&sessiontoken=$sessionToken';
    final response = await _dio.get(request);
    final body = response.data;

    if (response.statusCode == 200 && body['status'] == 'OK') {
      try {
        var location = body['result']['geometry']['location'];
        return model.copyWith(
          latitude: location['lat'] as double,
          longitude: location['lng'] as double,
        );
      } catch (e, stackTrace) {
        throw Exception(stackTrace);
      }
    } else {
      throw Exception(response.data['status']);
    }
  }
}
