// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class CategoryResponse {
  final int id;
  final String name;
  CategoryResponse({
    this.id = 0,
    this.name = '',
  });

  CategoryResponse copyWith({
    int? id,
    String? name,
  }) {
    return CategoryResponse(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
    };
  }

  factory CategoryResponse.fromMap(Map<String, dynamic> map) {
    return CategoryResponse(
      id: map['id'] as int,
      name: map['name'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory CategoryResponse.fromJson(String source) =>
      CategoryResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'CategoryResponse(id: $id, name: $name)';

  @override
  bool operator ==(covariant CategoryResponse other) {
    if (identical(this, other)) return true;

    return other.id == id && other.name == name;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode;
}
