// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/user/model/user_login_request.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/user/model/user_logout_request.dart';

import 'package:keringetan_consumer_flutter_app/features/login/data/model/login_response.dart';

abstract class UserService {
  Future<LoginResponse> signIn(UserLoginRequest request);
  Future<bool> signOut(UserLogoutRequest request);
}

@Injectable(as: UserService)
class UserServiceImpl implements UserService {
  final Dio dio;
  UserServiceImpl({
    required this.dio,
  });
  @override
  Future<LoginResponse> signIn(UserLoginRequest request) async {
    return await dio
        .post("/v1/api/login", data: request.toMap())
        .then((value) => LoginResponse.fromMap(value.data))
        .onError(
          (error, stackTrace) => Future.error(Exception(error.toString())),
        );
  }

  @override
  Future<bool> signOut(UserLogoutRequest request) async {
    return await dio
        .post("/v1/api/logout", data: request.toMap())
        .then((value) => true)
        .onError(
          (error, stackTrace) => Future.error(Exception(error.toString())),
        );
  }
}
