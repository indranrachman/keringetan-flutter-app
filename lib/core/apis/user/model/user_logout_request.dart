// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class UserLogoutRequest {
  final int id;
  UserLogoutRequest({
    this.id = 0,
  });

  UserLogoutRequest copyWith({
    int? id,
  }) {
    return UserLogoutRequest(
      id: id ?? this.id,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
    };
  }

  factory UserLogoutRequest.fromMap(Map<String, dynamic> map) {
    return UserLogoutRequest(
      id: map['id'] as int,
    );
  }

  String toJson() => json.encode(toMap());

  factory UserLogoutRequest.fromJson(String source) =>
      UserLogoutRequest.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'UserLogoutRequest(id: $id)';

  @override
  bool operator ==(covariant UserLogoutRequest other) {
    if (identical(this, other)) return true;

    return other.id == id;
  }

  @override
  int get hashCode => id.hashCode;
}
