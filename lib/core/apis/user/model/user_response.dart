// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class UserResponse {
  final int id;
  final String name;
  final String avatar;
  final String email;
  final String gender;
  final String bio;
  UserResponse({
    this.id = 0,
    this.name = '',
    this.avatar = '',
    this.email = '',
    this.gender = '',
    this.bio = '',
  });

  UserResponse copyWith({
    int? id,
    String? name,
    String? avatar,
    String? email,
    String? gender,
    String? bio,
  }) {
    return UserResponse(
      id: id ?? this.id,
      name: name ?? this.name,
      avatar: avatar ?? this.avatar,
      email: email ?? this.email,
      gender: gender ?? this.gender,
      bio: bio ?? this.bio,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'avatar': avatar,
      'email': email,
      'gender': gender,
      'bio': bio,
    };
  }

  factory UserResponse.fromMap(Map<String, dynamic> map) {
    return UserResponse(
      id: map['id'] as int,
      name: map['name'] as String,
      avatar: map['avatar'] as String,
      email: map['email'] as String,
      gender: map['gender'] as String,
      bio: map['bio'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory UserResponse.fromJson(String source) =>
      UserResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'UserResponse(id: $id, name: $name, avatar: $avatar, email: $email, gender: $gender, bio: $bio)';
  }

  @override
  bool operator ==(covariant UserResponse other) {
    if (identical(this, other)) return true;

    return other.id == id &&
        other.name == name &&
        other.avatar == avatar &&
        other.email == email &&
        other.gender == gender &&
        other.bio == bio;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        avatar.hashCode ^
        email.hashCode ^
        gender.hashCode ^
        bio.hashCode;
  }
}
