import 'dart:convert';

// ignore_for_file: public_member_api_docs, sort_constructors_first
class UserLoginRequest {
  final String email;
  final String name;
  UserLoginRequest({
    this.email = '',
    this.name = '',
  });
 

  UserLoginRequest copyWith({
    String? email,
    String? name,
  }) {
    return UserLoginRequest(
      email: email ?? this.email,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'email': email,
      'name': name,
    };
  }

  factory UserLoginRequest.fromMap(Map<String, dynamic> map) {
    return UserLoginRequest(
      email: map['email'] as String,
      name: map['name'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory UserLoginRequest.fromJson(String source) => UserLoginRequest.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'UserLoginRequest(email: $email, name: $name)';

  @override
  bool operator ==(covariant UserLoginRequest other) {
    if (identical(this, other)) return true;
  
    return 
      other.email == email &&
      other.name == name;
  }

  @override
  int get hashCode => email.hashCode ^ name.hashCode;
}
