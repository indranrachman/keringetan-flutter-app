import 'dart:convert';

class ParticipantResponse {
  final String id;
  final String name;
  final String avatar;
  ParticipantResponse({
    this.id = '',
    this.name = '',
    this.avatar = '',
  });

  ParticipantResponse copyWith({
    String? id,
    String? name,
    String? avatar,
  }) {
    return ParticipantResponse(
      id: id ?? this.id,
      name: name ?? this.name,
      avatar: avatar ?? this.avatar,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'avatar': avatar,
    };
  }

  factory ParticipantResponse.fromMap(Map<String, dynamic> map) {
    return ParticipantResponse(
      id: map['id'] as String,
      name: map['name'] as String,
      avatar: map['avatar'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory ParticipantResponse.fromJson(String source) =>
      ParticipantResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'ParticipantResponse(id: $id, name: $name, avatar: $avatar)';

  @override
  bool operator ==(covariant ParticipantResponse other) {
    if (identical(this, other)) return true;

    return other.id == id && other.name == name && other.avatar == avatar;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ avatar.hashCode;
}
