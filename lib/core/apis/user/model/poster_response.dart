// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class PosterResponse {
  final int id;
  final String name;
  final String avatar;
  PosterResponse({
    this.id = 0,
    this.name = '',
    this.avatar = '',
  });

  PosterResponse copyWith({
    int? id,
    String? name,
    String? avatar,
  }) {
    return PosterResponse(
      id: id ?? this.id,
      name: name ?? this.name,
      avatar: avatar ?? this.avatar,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'avatar': avatar,
    };
  }

  factory PosterResponse.fromMap(Map<String, dynamic> map) {
    return PosterResponse(
      id: map['id'] as int,
      name: map['name'] as String,
      avatar: map['avatar'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory PosterResponse.fromJson(String source) =>
      PosterResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'PosterResponse(id: $id, name: $name, avatar: $avatar)';

  @override
  bool operator ==(covariant PosterResponse other) {
    if (identical(this, other)) return true;

    return other.id == id && other.name == name && other.avatar == avatar;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ avatar.hashCode;
}
