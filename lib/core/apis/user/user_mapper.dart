import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/user/model/user_response.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';

abstract class UserMapper {
  UserModel toUserModel(UserResponse response);
}

@Injectable(as: UserMapper)
class UserMapperImpl implements UserMapper {
  @override
  UserModel toUserModel(UserResponse response) {
    return UserModel(
      name: response.name,
      email: response.email,
      bio: response.bio,
      gender: response.gender,
      avatar: response.avatar,
    );
  }
}
