// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

import 'package:keringetan_consumer_flutter_app/core/apis/event/model/request/create_event_request.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/model/request/edit_event_request.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/model/request/get_events_request.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/pref_utils.dart';
import 'package:keringetan_consumer_flutter_app/features/create_event/data/model/create_event_response.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/data/model/explore_response.dart';

abstract class EventService {
  Future<ExploreResponse> getEvents(GetEventsRequest request);
  Future<CreateEventResponse> createEvent(CreateEventRequest request);
  Future<CreateEventResponse> editEvent(EditEventRequest request);
}

@Injectable(as: EventService)
class EventServiceImpl implements EventService {
  final Dio dio;
  final PrefUtils prefUtils;
  EventServiceImpl({
    required this.dio,
    required this.prefUtils,
  });
  @override
  Future<ExploreResponse> getEvents(GetEventsRequest request) async {
    var userLat = prefUtils.getUserLat();
    var userLng = prefUtils.getUserLng();
    return await dio
        .get("/v1/api/events",
            queryParameters: request.toMap()
              ..removeWhere((key, value) => value == null),
            options: Options(
              headers: {
                "X-App-Lat": "$userLat",
                "X-App-Lng": "$userLng",
              },
            ))
        .then((value) => ExploreResponse.fromMap(value.data));
  }

  @override
  Future<CreateEventResponse> createEvent(CreateEventRequest request) async {
    return await dio
        .post("/v1/api/event", data: request.toMap())
        .then((value) => CreateEventResponse.fromMap(value.data));
  }

  @override
  Future<CreateEventResponse> editEvent(EditEventRequest request) async {
    return await dio
        .put("/v1/api/event/${request.id}", data: request.toMap())
        .then((value) => CreateEventResponse.fromMap(value.data));
  }
}
