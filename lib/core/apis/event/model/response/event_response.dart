// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:keringetan_consumer_flutter_app/core/apis/category/category_response.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/user/model/poster_response.dart';

class EventResponse {
  final int id;
  final String date;
  final String createdAt;
  final LocationResponse location;
  final String meetingNotes;
  final String caption;
  final String state;
  final String? imageUrl;
  final CategoryResponse category;
  final PosterResponse poster;
  EventResponse({
    this.id = 0,
    this.date = '',
    this.createdAt = '',
    required this.location,
    this.meetingNotes = '',
    this.caption = '',
    this.state = '',
    this.imageUrl,
    required this.category,
    required this.poster,
  });

  EventResponse copyWith({
    int? id,
    String? date,
    String? createdAt,
    LocationResponse? location,
    String? meetingNotes,
    String? caption,
    String? state,
    String? imageUrl,
    CategoryResponse? category,
    PosterResponse? poster,
  }) {
    return EventResponse(
      id: id ?? this.id,
      date: date ?? this.date,
      createdAt: createdAt ?? this.createdAt,
      location: location ?? this.location,
      meetingNotes: meetingNotes ?? this.meetingNotes,
      caption: caption ?? this.caption,
      state: state ?? this.state,
      imageUrl: imageUrl ?? this.imageUrl,
      category: category ?? this.category,
      poster: poster ?? this.poster,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'date': date,
      'createdAt': createdAt,
      'location': location.toMap(),
      'meetingNotes': meetingNotes,
      'caption': caption,
      'state': state,
      'imageUrl': imageUrl,
      'category': category.toMap(),
      'poster': poster.toMap(),
    };
  }

  factory EventResponse.fromMap(Map<String, dynamic> map) {
    return EventResponse(
      id: map['id'] as int,
      date: map['date'] as String,
      createdAt: map['createdAt'] as String,
      location:
          LocationResponse.fromMap(map['location'] as Map<String, dynamic>),
      meetingNotes: map['meetingNotes'] as String,
      caption: map['caption'] as String,
      state: map['state'] as String,
      imageUrl: map['imageUrl'] != null ? map['imageUrl'] as String : null,
      category:
          CategoryResponse.fromMap(map['category'] as Map<String, dynamic>),
      poster: PosterResponse.fromMap(map['poster'] as Map<String, dynamic>),
    );
  }

  String toJson() => json.encode(toMap());

  factory EventResponse.fromJson(String source) =>
      EventResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'EventResponse(id: $id, date: $date, createdAt: $createdAt, location: $location, meetingNotes: $meetingNotes, caption: $caption, state: $state, imageUrl: $imageUrl, category: $category, poster: $poster)';
  }

  @override
  bool operator ==(covariant EventResponse other) {
    if (identical(this, other)) return true;

    return other.id == id &&
        other.date == date &&
        other.createdAt == createdAt &&
        other.location == location &&
        other.meetingNotes == meetingNotes &&
        other.caption == caption &&
        other.state == state &&
        other.imageUrl == imageUrl &&
        other.category == category &&
        other.poster == poster;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        date.hashCode ^
        createdAt.hashCode ^
        location.hashCode ^
        meetingNotes.hashCode ^
        caption.hashCode ^
        state.hashCode ^
        imageUrl.hashCode ^
        category.hashCode ^
        poster.hashCode;
  }
}

class LocationResponse {
  final String name;
  final double latitude;
  final double longitude;
  final double? distance;
  LocationResponse({
    this.name = '',
    this.latitude = 0.0,
    this.longitude = 0.0,
    this.distance,
  });

  LocationResponse copyWith({
    String? name,
    double? latitude,
    double? longitude,
    double? distance,
  }) {
    return LocationResponse(
      name: name ?? this.name,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      distance: distance ?? this.distance,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'latitude': latitude,
      'longitude': longitude,
      'distance': distance,
    };
  }

  factory LocationResponse.fromMap(Map<String, dynamic> map) {
    return LocationResponse(
      name: map['name'] as String,
      latitude: map['latitude'] as double,
      longitude: map['longitude'] as double,
      distance: map['distance'] != null ? map['distance'] as double : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory LocationResponse.fromJson(String source) =>
      LocationResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'LocationResponse(name: $name, latitude: $latitude, longitude: $longitude, distance: $distance)';
  }

  @override
  bool operator ==(covariant LocationResponse other) {
    if (identical(this, other)) return true;

    return other.name == name &&
        other.latitude == latitude &&
        other.longitude == longitude &&
        other.distance == distance;
  }

  @override
  int get hashCode {
    return name.hashCode ^
        latitude.hashCode ^
        longitude.hashCode ^
        distance.hashCode;
  }
}
