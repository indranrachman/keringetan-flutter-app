// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class GetEventsRequest {
  final int limit;
  final int offset;
  final int? posterId;
  final int? distanceInKm;
  final int? categoryId;
  final String? state;
  GetEventsRequest({
    this.limit = 0,
    this.offset = 0,
    this.distanceInKm,
    this.posterId = 0,
    this.categoryId,
    this.state,
  });

  GetEventsRequest copyWith({
    int? limit,
    int? offset,
    int? distanceInKm,
    int? posterId,
    int? categoryId,
    String? state,
  }) {
    return GetEventsRequest(
      limit: limit ?? this.limit,
      offset: offset ?? this.offset,
      distanceInKm: distanceInKm ?? this.distanceInKm,
      posterId: posterId ?? this.posterId,
      categoryId: categoryId ?? this.categoryId,
      state: state ?? this.state,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'limit': limit,
      'offset': offset,
      'distanceInKm': distanceInKm,
      'posterId': posterId,
      'categoryId': categoryId,
      'state': state,
    };
  }

  factory GetEventsRequest.fromMap(Map<String, dynamic> map) {
    return GetEventsRequest(
      limit: map['limit'] as int,
      offset: map['offset'] as int,
      distanceInKm:
          map['distanceInKm'] != null ? map['distanceInKm'] as int : null,
      posterId: map['posterId'] as int,
      categoryId: map['categoryId'] != null ? map['categoryId'] as int : null,
      state: map['state'] != null ? map['state'] as String : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory GetEventsRequest.fromJson(String source) =>
      GetEventsRequest.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'GetEventsRequest(limit: $limit, offset: $offset, distanceInKm: $distanceInKm, posterId: $posterId, categoryId: $categoryId, state: $state)';
  }

  @override
  bool operator ==(covariant GetEventsRequest other) {
    if (identical(this, other)) return true;

    return other.limit == limit &&
        other.offset == offset &&
        other.distanceInKm == distanceInKm &&
        other.posterId == posterId &&
        other.categoryId == categoryId &&
        other.state == state;
  }

  @override
  int get hashCode {
    return limit.hashCode ^
        offset.hashCode ^
        distanceInKm.hashCode ^
        posterId.hashCode ^
        categoryId.hashCode ^
        state.hashCode;
  }
}
