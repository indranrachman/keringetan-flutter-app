// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class EditEventRequest {
  final int id;
  final int categoryId;
  final String caption;
  final String meetingNotes;
  final String date;
  final EditLocationRequest location;
  EditEventRequest({
    this.id = 0,
    this.categoryId = 0,
    this.caption = '',
    this.meetingNotes = '',
    this.date = '',
    required this.location,
  });

  EditEventRequest copyWith({
    int? id,
    int? categoryId,
    String? caption,
    String? meetingNotes,
    String? date,
    String? time,
    EditLocationRequest? location,
  }) {
    return EditEventRequest(
      id: id ?? this.id,
      categoryId: categoryId ?? this.categoryId,
      caption: caption ?? this.caption,
      meetingNotes: meetingNotes ?? this.meetingNotes,
      date: date ?? this.date,
      location: location ?? this.location,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'categoryId': categoryId,
      'caption': caption,
      'meetingNotes': meetingNotes,
      'date': date,
      'location': location.toMap(),
    };
  }

  factory EditEventRequest.fromMap(Map<String, dynamic> map) {
    return EditEventRequest(
      id: map['id'] as int,
      categoryId: map['categoryId'] as int,
      caption: map['caption'] as String,
      meetingNotes: map['meetingNotes'] as String,
      date: map['date'] as String,
      location:
          EditLocationRequest.fromMap(map['location'] as Map<String, dynamic>),
    );
  }

  String toJson() => json.encode(toMap());

  factory EditEventRequest.fromJson(String source) =>
      EditEventRequest.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'EditEventRequest(id: $id, categoryId: $categoryId, caption: $caption, meetingNotes: $meetingNotes, date: $date, location: $location)';
  }

  @override
  bool operator ==(covariant EditEventRequest other) {
    if (identical(this, other)) return true;

    return other.id == id &&
        other.categoryId == categoryId &&
        other.caption == caption &&
        other.meetingNotes == meetingNotes &&
        other.date == date &&
        other.location == location;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        categoryId.hashCode ^
        caption.hashCode ^
        meetingNotes.hashCode ^
        date.hashCode ^
        location.hashCode;
  }
}

class EditLocationRequest {
  final String name;
  final double latitude;
  final double longitude;
  EditLocationRequest({
    this.name = '',
    this.latitude = 0.0,
    this.longitude = 0.0,
  });

  EditLocationRequest copyWith({
    String? name,
    double? latitude,
    double? longitude,
  }) {
    return EditLocationRequest(
      name: name ?? this.name,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'latitude': latitude,
      'longitude': longitude,
    };
  }

  factory EditLocationRequest.fromMap(Map<String, dynamic> map) {
    return EditLocationRequest(
      name: map['name'] as String,
      latitude: map['latitude'] as double,
      longitude: map['longitude'] as double,
    );
  }

  String toJson() => json.encode(toMap());

  factory EditLocationRequest.fromJson(String source) =>
      EditLocationRequest.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'EditLocationRequest(name: $name, latitude: $latitude, longitude: $longitude)';

  @override
  bool operator ==(covariant EditLocationRequest other) {
    if (identical(this, other)) return true;

    return other.name == name &&
        other.latitude == latitude &&
        other.longitude == longitude;
  }

  @override
  int get hashCode => name.hashCode ^ latitude.hashCode ^ longitude.hashCode;
}
