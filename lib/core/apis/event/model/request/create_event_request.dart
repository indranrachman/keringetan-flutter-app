// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class CreateEventRequest {
  final int categoryId;
  final String caption;
  final String meetingNotes;
  final String date;
  final int posterId;
  final CreateEventLocationRequest location;
  CreateEventRequest({
    this.categoryId = 0,
    this.caption = '',
    this.meetingNotes = '',
    this.date = '',
    this.posterId = -1,
    required this.location,
  });

  CreateEventRequest copyWith({
    int? categoryId,
    String? caption,
    String? meetingNotes,
    String? date,
    int? posterId,
    CreateEventLocationRequest? location,
  }) {
    return CreateEventRequest(
      categoryId: categoryId ?? this.categoryId,
      caption: caption ?? this.caption,
      meetingNotes: meetingNotes ?? this.meetingNotes,
      date: date ?? this.date,
      posterId: posterId ?? this.posterId,
      location: location ?? this.location,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'categoryId': categoryId,
      'caption': caption,
      'meetingNotes': meetingNotes,
      'date': date,
      'posterId': posterId,
      'location': location.toMap(),
    };
  }

  factory CreateEventRequest.fromMap(Map<String, dynamic> map) {
    return CreateEventRequest(
      categoryId: map['categoryId'] as int,
      caption: map['caption'] as String,
      meetingNotes: map['meetingNotes'] as String,
      date: map['date'] as String,
      posterId: map['posterId'] as int,
      location: CreateEventLocationRequest.fromMap(
          map['location'] as Map<String, dynamic>),
    );
  }

  String toJson() => json.encode(toMap());

  factory CreateEventRequest.fromJson(String source) =>
      CreateEventRequest.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'CreateEventRequest(categoryId: $categoryId, caption: $caption, meetingNotes: $meetingNotes, date: $date, posterId: $posterId, location: $location)';
  }

  @override
  bool operator ==(covariant CreateEventRequest other) {
    if (identical(this, other)) return true;

    return other.categoryId == categoryId &&
        other.caption == caption &&
        other.meetingNotes == meetingNotes &&
        other.date == date &&
        other.posterId == posterId &&
        other.location == location;
  }

  @override
  int get hashCode {
    return categoryId.hashCode ^
        caption.hashCode ^
        meetingNotes.hashCode ^
        date.hashCode ^
        posterId.hashCode ^
        location.hashCode;
  }
}

class CreateEventLocationRequest {
  final String name;
  final double latitude;
  final double longitude;
  CreateEventLocationRequest({
    this.name = '',
    this.latitude = 0.0,
    this.longitude = 0.0,
  });

  CreateEventLocationRequest copyWith({
    String? name,
    double? latitude,
    double? longitude,
  }) {
    return CreateEventLocationRequest(
      name: name ?? this.name,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'latitude': latitude,
      'longitude': longitude,
    };
  }

  factory CreateEventLocationRequest.fromMap(Map<String, dynamic> map) {
    return CreateEventLocationRequest(
      name: map['name'] as String,
      latitude: map['latitude'] as double,
      longitude: map['longitude'] as double,
    );
  }

  String toJson() => json.encode(toMap());

  factory CreateEventLocationRequest.fromJson(String source) =>
      CreateEventLocationRequest.fromMap(
          json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'CreateEventLocationRequest(name: $name, latitude: $latitude, longitude: $longitude)';

  @override
  bool operator ==(covariant CreateEventLocationRequest other) {
    if (identical(this, other)) return true;

    return other.name == name &&
        other.latitude == latitude &&
        other.longitude == longitude;
  }

  @override
  int get hashCode => name.hashCode ^ latitude.hashCode ^ longitude.hashCode;
}
