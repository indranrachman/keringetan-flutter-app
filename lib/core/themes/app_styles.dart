import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';

class AppStyles {
  static TextStyle anekLatinBold = GoogleFonts.poppins(
    color: AppColors.textPrimary,
    fontWeight: FontWeight.bold,
  );

  static TextStyle anekLatinRegular = GoogleFonts.poppins(
    color: AppColors.textPrimary,
    fontWeight: FontWeight.normal,
  );

  static TextStyle anekLatinSemiBold = GoogleFonts.poppins(
    color: AppColors.textPrimary,
    fontWeight: FontWeight.w600,
  );

  static ButtonStyle primaryButton = ElevatedButton.styleFrom(
    backgroundColor: AppColors.primary,
    textStyle: anekLatinBold.copyWith(color: AppColors.light),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(8.0),
      ),
    ),
  );

  static ButtonStyle secondaryButton = OutlinedButton.styleFrom(
    backgroundColor: AppColors.pillBlueBackground,
    textStyle: anekLatinBold.copyWith(
      color: AppColors.primary,
    ),
    side: BorderSide.none,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(8.0),
      ),
    ),
  );

  static ButtonStyle primaryButtonInverted = ElevatedButton.styleFrom(
    backgroundColor: AppColors.light,
    textStyle: anekLatinBold.copyWith(color: AppColors.textPrimary),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(8.0),
      ),
    ),
  );
}
