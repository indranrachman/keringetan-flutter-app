import 'package:flutter/material.dart';

class AppColors {
  static Color primary = const Color(0xFF1243C1);

  static Color secondary = const Color(0xFFe3f2fd);

  static Color accent = const Color(0xFFfca311);

  static Color light = const Color(0xFFFFFFFF);

  static Color textPrimary = const Color(0xFF0C1120);

  static Color textAccent = const Color(0xFF2D8FC7);

  static Color textLight = const Color(0xFFFFFFFF);

  static Color textCaption = const Color(0xFF495057);

  static Color textHint = const Color(0x90495057);

  static Color cardBorder = const Color(0xFFced4da);

  static Color cardBorderActive = const Color(0xFF1243C1);

  static Color cardBackground = const Color(0xFFFFFFFF);

  static Color cardBackgroundActive = const Color.fromARGB(255, 241, 240, 240);

  static Color viewDivider = const Color(0xFFCCCCCC);

  static Color pillGreenBackground = const Color(0xFFE4F7ED);

  static Color pillGreenText = const Color(0xFF3AB971);

  static Color pillOrangeBackground = const Color(0xFFFEF8E6);

  static Color pillOrangeText = const Color(0xFFFEB800);

  static Color pillBlueBackground = const Color(0xFFECF1FF);

  static Color pillBlueText = const Color(0xFF4C70FF);

  static Color pillMutedBackground = const Color.fromARGB(255, 241, 240, 240);

  static Color pillMutedText = const Color(0xFF495057);

  static Color bottomSheetBarrierColor = Colors.black.withOpacity(0.75);
}
