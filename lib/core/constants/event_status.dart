enum EventStatus { open, byYou, joined, unknown }

class EventStatusUtils {
  static EventStatus fromString(String statusName) {
    switch (statusName) {
      case "NEARBY":
        return EventStatus.open;
      case "MADE_BY_YOU":
        return EventStatus.byYou;
      case "JOINED":
        return EventStatus.joined;
      default:
        return EventStatus.unknown;
    }
  }
}
