/*
Event Collection START
*/

const getEventPath = "/v1/events";
const createEventPath = "/v1/event";
const editEventPath = "/v1/events/{id}";
const joinEventPath = "/v1/events/{id}";

/*
Event Collection END
*/