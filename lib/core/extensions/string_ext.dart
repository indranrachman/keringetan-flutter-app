extension StringExt on String? {
  String orEmpty() {
    return this ?? "";
  }
}
