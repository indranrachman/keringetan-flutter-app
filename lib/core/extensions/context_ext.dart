import 'package:flutter/material.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';

extension ContextExt on BuildContext {
  showSnackBar(String? message) {
    if (message != null && message.isNotEmpty) {
      ScaffoldMessenger.of(this).showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          duration: const Duration(seconds: 60),
          content: Text(
            message,
            style: AppStyles.anekLatinSemiBold.copyWith(
              color: AppColors.light,
            ),
          ),
          showCloseIcon: true,
          closeIconColor: AppColors.light,
          backgroundColor: AppColors.primary,
        ),
      );
    }
  }
}
