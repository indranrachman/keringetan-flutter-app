import 'package:keringetan_consumer_flutter_app/core/apis/google/place/model/place_suggestion_model.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_filter_model.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';
import 'package:keringetan_consumer_flutter_app/features/notifications/presentation/model/notification_model.dart';

class DataFaker {
  // static List<EventResponse> eventResponse = [
  //   EventResponse(
  //     participantIds: [1],
  //     id: 1,
  //     category: CategoryResponse(id: "0", name: "Cycling"),
  //     date: "Saturday, 22 May 2023",
  //     createdAt: "",
  //     location: "Universitas Indonesia",
  //     meetingNotes: "Parkiran FISIP",
  //     status: "JOINED",
  //     caption:
  //         "Guys temenin lari dong, gw kesepian nih. Dari kemarin lagi di tinggal sama temen2 gw sekolah ke LN. Malesin.",
  //     poster: PosterResponse(
  //       id: "id-1",
  //       name: "Indra Nur Rachman",
  //       avatar:
  //           "https://media.licdn.com/dms/image/D5603AQG4ZL7bLvQAEA/profile-displayphoto-shrink_800_800/0/1681890089223?e=2147483647&v=beta&t=_idTZsQ7Xzv-S1nkvznBPttgoHGPt81yLfzWO7o0WS4",
  //     ),
  //   ),
  //   EventResponse(
  //     participantIds: [1],
  //     id: "chat-1",
  //     category: CategoryResponse(id: "3", name: "Badminton"),
  //     date: "Saturday, 23 May 2023",
  //     location: "Politeknik Negeri Jakarta",
  //     meetingNotes: "Depan GSG",
  //     status: "BY_YOU",
  //     caption:
  //         "Guys temenin lari dong. Pokoknya langsung aja join udah auto gw accept buruan yah",
  //     poster: PosterResponse(
  //       id: "id-2",
  //       name: "Tiffani Shavira Arnetha",
  //       avatar:
  //           "https://res.cloudinary.com/dk0z4ums3/image/upload/w_130,h_130,c_fill/v1585768849/user_images/5c108c1922ad5b09d5ce6fc8.jpg",
  //     ),
  //   ),
  //   EventResponse(
  //     participantIds: [1],
  //     id: "chat-1",
  //     category: CategoryResponse(id: "0", name: "Cycling"),
  //     date: "Monday, 6 Jun 2023",
  //     status: "BY_YOU",
  //     location: "Universitas Indonesia",
  //     meetingNotes: "Parkiran Fasilkom UI",
  //     caption: "Guys temenin lari dong, gw kesepian nih HUHUHU",
  //     poster: PosterResponse(
  //       id: "id-1",
  //       name: "Karenia Emeralda",
  //       avatar:
  //           "https://media.licdn.com/dms/image/C5103AQGCLWXC0n7Ivw/profile-displayphoto-shrink_800_800/0/1581084909086?e=2147483647&v=beta&t=nGrDKDj0uw97OqtIZgumEKGhd7B0hfmVjUlwmdIFYKo",
  //     ),
  //   ),
  //   EventResponse(
  //     participantIds: [1],
  //     id: "chat-1",
  //     category: CategoryResponse(id: "1", name: "Running"),
  //     date: "Wednesday, 22 Oct 2023",
  //     status: "OPEN",
  //     location: "Grand Depok City",
  //     meetingNotes: "Depan JGU",
  //     caption: "Dari kemarin lagi di tinggal sama temen2 gw sekolah ke LN.",
  //     poster: PosterResponse(
  //       id: "id-3",
  //       name: "Imam Taufik",
  //       avatar:
  //           "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTCtkOWgA0kCVlE70Ng8nB5sqb9rJX2XeiY-w&usqp=CAU",
  //     ),
  //   ),
  //   EventResponse(
  //     participantIds: [1],
  //     id: "chat-1",
  //     category: CategoryResponse(id: "3", name: "Badminton"),
  //     date: "Sunday, 22 Sept 2023",
  //     location: "Gudang Sarinah",
  //     status: "OPEN",
  //     meetingNotes: "Parkiran Utama",
  //     caption:
  //         "Guys temenin lari dong, gw kesepian nih. Dari kemarin lagi di tinggal sama temen2 gw sekolah ke LN. Malesin. Pokoknya langsung aja join udah auto gw accept buruan yah",
  //     poster: PosterResponse(
  //       id: "id-4",
  //       name: "Erlita Kusuma",
  //       avatar: "https://cuad.ask.fm/assets2/071/473/345/536/normal/avatar.jpg",
  //     ),
  //   )
  // ];

  static List<CategoryModel> categories = [
    CategoryModel(
      id: 1,
      name: "Cycling",
    ),
    CategoryModel(
      id: 2,
      name: "Running",
    ),
    CategoryModel(
      id: 3,
      name: "Futsal",
    ),
    CategoryModel(
      id: 4,
      name: "Badminton",
    ),
  ];

  static List<PlaceSuggestionModel> locations = [
    PlaceSuggestionModel(
      placeId: "0",
      name: "Universitas Indonesia",
      description: "Tirtajaya, Kota Depok, Jawa Barat, Indonesia",
    ),
    PlaceSuggestionModel(
      placeId: "0",
      name: "Politeknik Negeri Jakarta",
      description: "Beji, Kota Depok, Jawa Barat, Indonesia",
    ),
    PlaceSuggestionModel(
      placeId: "0",
      name: "Universitas Indonesia",
      description: "Tirtajaya, Kota Depok, Jawa Barat, Indonesia",
    ),
    PlaceSuggestionModel(
      placeId: "0",
      name: "Universitas Indonesia",
      description: "Tirtajaya, Kota Depok, Jawa Barat, Indonesia",
    ),
    PlaceSuggestionModel(
      placeId: "0",
      name: "Universitas Indonesia",
      description: "Tirtajaya, Kota Depok, Jawa Barat, Indonesia",
    ),
  ];

  static List<ExploreFilterModel> categoryFilters = [
    ExploreFilterModel(
      id: "1",
      name: "Cycling",
      isSelected: false,
    ),
    ExploreFilterModel(
      id: "2",
      name: "Running",
      isSelected: false,
    ),
    ExploreFilterModel(
      id: "3",
      name: "Futsal",
      isSelected: false,
    ),
    ExploreFilterModel(
      id: "4",
      name: "Badminton",
      isSelected: false,
    ),
  ];
  static List<ExploreFilterModel> statusFilters = [
    ExploreFilterModel(
      id: "MADE_BY_YOU",
      name: "Made by you",
      isSelected: false,
    ),
    ExploreFilterModel(
      id: "JOINED",
      name: "Joined",
      isSelected: false,
    ),
  ];

  static List<NotificationModel> notifications = [
    NotificationModel(
        id: "id",
        title: "You have incoming event in an hour GEGEGEGGEEGE",
        subTitle:
            "Hi there, you have cycling event with Tiffani in an hour, please prepared for a joyful one",
        timeStamp: "2mins ago",
        deeplink: "deeplink-to-event-detail",
        isRead: false),
    NotificationModel(
        id: "id",
        title: "You have incoming event in an hour",
        subTitle:
            "Hi there, you have cycling event with Tiffani in an hour, please prepared for a joyful one",
        timeStamp: "2mins ago",
        deeplink: "deeplink-to-event-detail",
        isRead: false),
    NotificationModel(
      id: "id",
      title: "You have incoming event in an hour",
      subTitle:
          "Hi there, you have cycling event with Tiffani in an hour, please prepared for a joyful one",
      timeStamp: "2mins ago",
      deeplink: "deeplink-to-event-detail",
      isRead: false,
    ),
  ];
}
