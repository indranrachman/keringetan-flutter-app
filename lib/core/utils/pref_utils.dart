import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

// ignore_for_file: must_be_immutable
class PrefUtils {
  static const userId = "USER-ID";
  static const userName = "USER-NAME";
  static const user = "USER";
  static const token = "TOKEN";
  static const userLat = "USER-LAT";
  static const userLng = "USER-LNG";
  static const userRecentLocationSearch = "USER-RECENT-LOCATION-SEARCH";

  SharedPreferences? _sharedPreferences;

  Future<void> init() async {
    _sharedPreferences = await SharedPreferences.getInstance();
    if (_sharedPreferences != null) {
      if (kDebugMode) {
        print('Sucessfully initialized SharedPreference');
      }
    } else {
      if (kDebugMode) {
        print('Failed to initialized SharedPreference');
      }
    }
  }

  ///will clear all the data stored in preference
  void clearPreferencesData() async {
    _sharedPreferences!.clear();
  }

  Future<void> setUserId(int value) {
    return _sharedPreferences!.setInt(userId, value);
  }

  int? getUserId() {
    try {
      return _sharedPreferences!.getInt(userId);
    } catch (e) {
      return null;
    }
  }

  setToken(String value) {
    return _sharedPreferences!.setString(token, value);
  }

  String getToken() {
    try {
      return _sharedPreferences!.getString(token) ?? '';
    } catch (e) {
      return '';
    }
  }

  setUserName(String value) {
    return _sharedPreferences!.setString(userName, value);
  }

  String getUserName() {
    try {
      return _sharedPreferences!.getString(userName) ?? '';
    } catch (e) {
      return '';
    }
  }

  Future<void> setUser(String value) {
    return _sharedPreferences!.setString(user, value);
  }

  Future<String> getUser() async {
    try {
      return _sharedPreferences!.getString(user) ?? "";
    } catch (e) {
      return "";
    }
  }

  setUserLat(double value) {
    return _sharedPreferences!.setDouble(userLat, value);
  }

  getUserLat() {
    try {
      return _sharedPreferences!.getDouble(userLat) ?? 0.0;
    } catch (e) {
      return 0.0;
    }
  }

  setUserLng(double value) {
    return _sharedPreferences!.setDouble(userLng, value);
  }

  getUserLng() {
    try {
      return _sharedPreferences!.getDouble(userLng) ?? 0.0;
    } catch (e) {
      return 0.0;
    }
  }

  setRecentLocationSearch(String value) {
    return _sharedPreferences!.setString(userRecentLocationSearch, value);
  }

  getRecentLocationSearch() {
    try {
      return _sharedPreferences!.getString(userRecentLocationSearch) ?? '';
    } catch (e) {
      return '';
    }
  }
}
