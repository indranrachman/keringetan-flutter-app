import 'dart:io';

import 'package:image_picker/image_picker.dart';

class ImagePickerUtils {
  static Future<File?> open(ImageSource source) async {
    final image = await ImagePicker().pickImage(source: source);
    if (image != null) {
      final imageTemp = File(image.path);
      return imageTemp;
    }
    return null;
  }
}
