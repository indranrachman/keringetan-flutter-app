// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:geolocator/geolocator.dart';
import 'package:injectable/injectable.dart';

import 'package:keringetan_consumer_flutter_app/core/utils/pref_utils.dart';

abstract class UserLocationService {
  Future<void> requestUserLocation();
}

@Injectable(as: UserLocationService)
class UserLocationServiceImpl implements UserLocationService {
  final PrefUtils prefUtils;
  UserLocationServiceImpl({
    required this.prefUtils,
  });

  @override
  Future<void> requestUserLocation() async {
    var serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error(
          "Location services are disabled. Please enable the services");
    }
    var permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error("Location permissions are denied");
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    var position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
    prefUtils.setUserLat(position.latitude);
    prefUtils.setUserLng(position.longitude);
    return;
  }
}
