import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/google/place/model/place_suggestion_model.dart';
import 'package:keringetan_consumer_flutter_app/core/deps/dependency_injection.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';
import 'package:keringetan_consumer_flutter_app/features/empty/ui/empty_state.dart';
import 'package:keringetan_consumer_flutter_app/features/location_picker/presentation/bloc/location_picker_bloc.dart';

class LocationPickerPage extends StatefulWidget {
  final Function(PlaceSuggestionModel model) onLocationSelected;

  const LocationPickerPage({
    super.key,
    required this.onLocationSelected,
  });

  static Widget builder(
    BuildContext context,
    Function(PlaceSuggestionModel model) onLocationSelected,
  ) {
    return BlocProvider<LocationPickerBloc>(
      create: (context) =>
          getIt<LocationPickerBloc>()..add(FetchRecentLocatioSearchEvent()),
      child: LocationPickerPage(
        onLocationSelected: onLocationSelected,
      ),
    );
  }

  @override
  State<LocationPickerPage> createState() => _LocationPickerPageState();
}

class _LocationPickerPageState extends State<LocationPickerPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      maintainBottomViewPadding: true,
      child: Container(
        color: AppColors.light,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 16),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Choose Locations',
                  style: AppStyles.anekLatinBold,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16),
              child: TextField(
                maxLines: 1,
                textInputAction: TextInputAction.go,
                onSubmitted: (value) {
                  if (value.isNotEmpty) {
                    context.read<LocationPickerBloc>().add(
                          SendLocationQueryEvent(
                            query: value,
                            countryCode: 'id',
                          ),
                        );
                  }
                },
                style: AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                cursorColor: AppColors.primary,
                decoration: InputDecoration(
                  prefixIcon: const Icon(Icons.search_rounded),
                  prefixIconColor: AppColors.primary,
                  labelText: "Search your keyword here..",
                  labelStyle: AppStyles.anekLatinRegular.copyWith(
                    fontSize: 12,
                    color: AppColors.textHint,
                  ),
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  floatingLabelStyle: AppStyles.anekLatinRegular.copyWith(
                    color: AppColors.primary,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: AppColors.primary, width: 1),
                    borderRadius: const BorderRadius.all(Radius.circular(8)),
                  ),
                  isDense: true,
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                        BorderSide(color: AppColors.cardBorder, width: 1),
                    borderRadius: const BorderRadius.all(Radius.circular(8)),
                  ),
                ),
                enableSuggestions: false,
              ),
            ),
            const Divider(
              height: 2,
            ),
            BlocConsumer<LocationPickerBloc, LocationPickerState>(
              listener: (context, state) {
                if (state is LocationPickerPlaceDetailSuccessResult) {
                  widget.onLocationSelected(state.location);
                  Navigator.pop(context);
                }
              },
              builder: (context, state) {
                return _handleContent(state);
              },
            )
          ],
        ),
      ),
    );
  }

  Widget _handleContent(LocationPickerState state) {
    if (state is LocationPickerEmptyResult) {
      return SizedBox(
        height: MediaQuery.of(context).size.height / 2,
        child: Center(
          child: EmptyState(
            model: EmptyStateModel(
              title: 'No Location Found',
              subTitle: 'Type something to find another location',
            ),
          ),
        ),
      );
    } else if (state is LocationPickerSuccessResult) {
      return ListView.separated(
        physics: const NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: state.locations.length,
        separatorBuilder: (context, index) {
          return const Padding(
            padding: EdgeInsets.only(left: 16, right: 16),
            child: Divider(height: 2),
          );
        },
        itemBuilder: (context, index) {
          return InkWell(
            onTap: () {
              setState(() {
                state.locations[index].isLoading = true;
              });
              context.read<LocationPickerBloc>().add(
                    FetchLocationDetailsEvent(
                      model: state.locations[index],
                    ),
                  );
            },
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Row(
                children: [
                  Container(
                    width: 36,
                    height: 36,
                    decoration: BoxDecoration(
                      color: AppColors.pillMutedBackground,
                      shape: BoxShape.circle,
                    ),
                    child: Icon(
                      size: 16,
                      Icons.pin_drop,
                      color: AppColors.primary,
                    ),
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          state.locations[index].name,
                          style: AppStyles.anekLatinSemiBold.copyWith(
                            fontSize: 12,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        const SizedBox(height: 4),
                        Text(
                          state.locations[index].description,
                          style: AppStyles.anekLatinRegular.copyWith(
                            fontSize: 12,
                          ),
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 36,
                    height: 36,
                    child: Center(
                      child: Visibility(
                        visible: state.locations[index].isLoading,
                        child: const CircularProgressIndicator.adaptive(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      );
    } else if (state is LocationPickerFailedResult) {
      return SizedBox(
        height: MediaQuery.of(context).size.height * 0.5,
        child: Center(
          child: EmptyState(
            model: EmptyStateModel(
              title: 'Something Went Wrong',
              subTitle: state.errorMessage,
            ),
          ),
        ),
      );
    } else if (state is LocationPickerInitial) {
      return SizedBox(
        height: MediaQuery.of(context).size.height * 0.5,
        child: Center(
          child: EmptyState(
            model: EmptyStateModel(
              title: 'Find Nearest Places',
              subTitle: 'Location will be shown here',
            ),
          ),
        ),
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height * 0.5,
        child: const Center(
          child: CircularProgressIndicator.adaptive(),
        ),
      );
    }
  }
}
