// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'location_picker_bloc.dart';

abstract class LocationPickerState extends Equatable {
  const LocationPickerState();
  @override
  List<Object> get props => [];
}

class LocationPickerInitial extends LocationPickerState {}

class LocationPickerShowLoading extends LocationPickerState {}

class LocationPickerEmptyResult extends LocationPickerState {}

class LocationPickerPlaceDetailSuccessResult extends LocationPickerState {
  final PlaceSuggestionModel location;
  const LocationPickerPlaceDetailSuccessResult({
    required this.location,
  });
}

class LocationPickerSuccessResult extends LocationPickerState {
  final List<PlaceSuggestionModel> locations;
  const LocationPickerSuccessResult({
    this.locations = const [],
  });
}

class LocationPickerFailedResult extends LocationPickerState {
  final String errorMessage;
  const LocationPickerFailedResult({
    this.errorMessage = '',
  });
}
