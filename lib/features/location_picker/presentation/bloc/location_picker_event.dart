// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'location_picker_bloc.dart';

abstract class LocationPickerEvent extends Equatable {
  const LocationPickerEvent();

  @override
  List<Object> get props => [];
}

class FetchRecentLocatioSearchEvent extends LocationPickerEvent {}

class FetchLocationDetailsEvent extends LocationPickerEvent {
  final PlaceSuggestionModel model;
  const FetchLocationDetailsEvent({
    required this.model,
  });
}

class SendLocationQueryEvent extends LocationPickerEvent {
  final String query;
  final String countryCode;
  const SendLocationQueryEvent({
    this.query = '',
    this.countryCode = '',
  });
}
