import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/google/place/model/place_suggestion_model.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/google/place/place_service.dart';

part 'location_picker_event.dart';
part 'location_picker_state.dart';

@injectable
class LocationPickerBloc
    extends Bloc<LocationPickerEvent, LocationPickerState> {
  final PlaceService placeService;

  LocationPickerBloc(this.placeService) : super(LocationPickerInitial()) {
    on<FetchLocationDetailsEvent>((event, emit) async {
      await placeService
          .fetchPlaceDetails(event.model)
          .then((value) =>
              emit(LocationPickerPlaceDetailSuccessResult(location: value)))
          .onError((error, stackTrace) =>
              emit(LocationPickerFailedResult(errorMessage: error.toString())));
    });
    on<FetchRecentLocatioSearchEvent>((event, emit) async {
      var locations = placeService.fetchRecentSearches();
      if (locations.isEmpty) {
        emit(LocationPickerEmptyResult());
      } else {
        emit(LocationPickerSuccessResult(locations: locations));
      }
    });
    on<SendLocationQueryEvent>((event, emit) async {
      emit(LocationPickerShowLoading());
      await placeService
          .fetchPlaces(event.query, event.countryCode)
          .then((locations) =>
              emit(LocationPickerSuccessResult(locations: locations)))
          .onError((error, stackTrace) =>
              emit(LocationPickerFailedResult(errorMessage: error.toString())));
    });
  }
}
