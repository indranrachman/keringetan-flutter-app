import 'package:flutter/material.dart';
import 'package:keringetan_consumer_flutter_app/features/create_event/ui/create_event_page.dart';
import 'package:keringetan_consumer_flutter_app/features/empty/ui/empty_state.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/ui/explore_page.dart';
import 'package:keringetan_consumer_flutter_app/features/profile/ui/profile_page.dart';

import '../../../core/routes/app_routes.dart';
import '../presentation/main_bottom_navigation_type.dart';

class MainBottomNavigationRouterUtils {
  static Widget getPageByRoute(
    BuildContext context,
    String currentRoute,
  ) {
    switch (currentRoute) {
      case AppRoutes.explorePage:
        return ExplorePage.builder(context);
      case AppRoutes.myActivitiesPage:
        return CreateEventPage.builder(context);
      case AppRoutes.profilePage:
        return ProfilePage.builder(context);
      default:
        return EmptyState(
          model: EmptyStateModel(
              title: "No routes found", subTitle: "currentRoute"),
        );
    }
  }

  static String getRouteByType(
    MainBottomNavigationType type,
  ) {
    switch (type) {
      case MainBottomNavigationType.explore:
        return AppRoutes.explorePage;
      case MainBottomNavigationType.createEvent:
        return AppRoutes.myActivitiesPage;
      case MainBottomNavigationType.profile:
        return AppRoutes.profilePage;
      default:
        return "/";
    }
  }
}
