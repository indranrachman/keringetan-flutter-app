// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';

import '../../presentation/main_bottom_navigation_type.dart';

class MainBottomNavigationBar extends StatefulWidget {
  final Function(MainBottomNavigationType type) onMenuClicked;
  final Function() onCreateEventMenuClicked;

  const MainBottomNavigationBar({
    Key? key,
    required this.onMenuClicked,
    required this.onCreateEventMenuClicked,
  }) : super(key: key);

  @override
  State<MainBottomNavigationBar> createState() =>
      _MainBottomNavigationBarState();
}

class _MainBottomNavigationBarState extends State<MainBottomNavigationBar> {
  var _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 0.5,
          ),
        ],
      ),
      child: BottomNavigationBar(
        backgroundColor: AppColors.light,
        currentIndex: _currentIndex,
        onTap: (index) {
          if (_currentIndex == index) {
            return;
          }
          var type = _getPageType(index);
          if (type == MainBottomNavigationType.createEvent) {
            widget.onCreateEventMenuClicked();
          } else {
            widget.onMenuClicked(type);
            setState(() {
              _currentIndex = index;
            });
          }
        },
        type: BottomNavigationBarType.fixed,
        selectedItemColor: AppColors.primary,
        selectedLabelStyle: AppStyles.anekLatinBold,
        unselectedLabelStyle: AppStyles.anekLatinRegular,
        items: const [
          BottomNavigationBarItem(
            label: "Explore",
            activeIcon: Padding(
              padding: EdgeInsets.only(bottom: 4.0),
              child: Icon(Icons.explore),
            ),
            icon: Padding(
              padding: EdgeInsets.only(bottom: 4.0),
              child: Icon(Icons.explore),
            ),
          ),
          BottomNavigationBarItem(
            label: "Event",
            activeIcon: Padding(
              padding: EdgeInsets.only(bottom: 4.0),
              child: Icon(Icons.add_circle),
            ),
            icon: Padding(
              padding: EdgeInsets.only(bottom: 4.0),
              child: Icon(Icons.add_circle),
            ),
          ),
          BottomNavigationBarItem(
            label: "My Profile",
            activeIcon: Padding(
              padding: EdgeInsets.only(bottom: 4.0),
              child: Icon(Icons.face),
            ),
            icon: Padding(
              padding: EdgeInsets.only(bottom: 4.0),
              child: Icon(Icons.face),
            ),
          ),
        ],
      ),
    );
  }
}

MainBottomNavigationType _getPageType(int index) {
  switch (index) {
    case 0:
      return MainBottomNavigationType.explore;
    case 1:
      return MainBottomNavigationType.createEvent;
    case 2:
      return MainBottomNavigationType.profile;
    default:
      return MainBottomNavigationType.explore;
  }
}
