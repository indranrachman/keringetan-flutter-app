import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keringetan_consumer_flutter_app/core/extensions/context_ext.dart';
import 'package:keringetan_consumer_flutter_app/core/routes/app_routes.dart';
import 'package:keringetan_consumer_flutter_app/features/create_event/ui/create_event_page.dart';
import 'package:keringetan_consumer_flutter_app/features/main/presentation/bloc/main_bloc.dart';
import 'package:keringetan_consumer_flutter_app/features/main/utils/main_bottom_navigation_router_utils.dart';

import 'widgets/main_bottom_navigation_bar.dart';

// ignore_for_file: must_be_immutable
class MainPage extends StatefulWidget {
  const MainPage({super.key});

  static Widget builder(BuildContext context) {
    return BlocProvider<MainBloc>(
      create: (context) => MainBloc(),
      child: const MainPage(),
    );
  }

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with TickerProviderStateMixin {
  GlobalKey<NavigatorState> mainNavigatorKey = GlobalKey();

  late AnimationController controller;

  @override
  initState() {
    super.initState();
    initController();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void initController() {
    controller = BottomSheet.createAnimationController(this);
    controller.duration = const Duration(milliseconds: 350);
    controller.reverseDuration = const Duration(milliseconds: 200);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: Colors.transparent,
        body: _determinePage(),
        bottomNavigationBar: MainBottomNavigationBar(
          onCreateEventMenuClicked: () async {
            Navigator.of(context, rootNavigator: true)
                .push(
                  MaterialPageRoute(
                    fullscreenDialog: true,
                    builder: (context) {
                      return CreateEventPage.builder(context);
                    },
                  ),
                )
                .then((value) => {context.showSnackBar(value)});
          },
          onMenuClicked: (type) {
            Navigator.pushNamed(
              mainNavigatorKey.currentContext!,
              MainBottomNavigationRouterUtils.getRouteByType(type),
            );
          },
        ),
      ),
    );
  }

  Widget _determinePage() {
    return Navigator(
        key: mainNavigatorKey,
        initialRoute: AppRoutes.explorePage,
        onGenerateRoute: (routeSetting) {
          return PageRouteBuilder(
            pageBuilder: (context, _, __) {
              return MainBottomNavigationRouterUtils.getPageByRoute(
                context,
                routeSetting.name!,
              );
            },
            transitionDuration: const Duration(milliseconds: 200),
          );
        });
  }
}
