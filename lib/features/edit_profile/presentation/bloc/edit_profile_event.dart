// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'edit_profile_bloc.dart';

abstract class EditProfileEvent {
  const EditProfileEvent();
}

class SendEditProfileEvent extends EditProfileEvent {
  final String name;
  final String bio;
  final String avatar;
  final String gender;
  SendEditProfileEvent({
    this.name = '',
    this.bio = '',
    this.avatar = '',
    this.gender = '',
  });
}

class UploadImageToS3BucketEvent extends EditProfileEvent {
  File image;
  UploadImageToS3BucketEvent({
    required this.image,
  });
}
