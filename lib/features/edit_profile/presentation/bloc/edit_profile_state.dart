part of 'edit_profile_bloc.dart';

abstract class EditProfileState extends Equatable {
  const EditProfileState();

  @override
  List<Object> get props => [];
}

class EditProfileLoadingState extends EditProfileState {
  final bool show;

  const EditProfileLoadingState(this.show);
}

class EditProfileInitial extends EditProfileState {}

class EditProfileSuccessState extends EditProfileState {}

class EditProfileFailed extends EditProfileState {
  final String errorMessage;

  const EditProfileFailed(this.errorMessage);
}

class UploadAvatarLoadingState extends EditProfileState {
  final bool show;

  const UploadAvatarLoadingState(this.show);
}

class UploadAvatarSuccessState extends EditProfileState {
  final String avatarUrl;

  const UploadAvatarSuccessState(this.avatarUrl);
}

class UploadAvatarFailed extends EditProfileState {
  final String errorMessage;

  const UploadAvatarFailed(this.errorMessage);
}
