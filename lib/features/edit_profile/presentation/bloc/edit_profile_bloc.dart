import 'dart:io';

import 'package:aws_s3_upload/aws_s3_upload.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

part 'edit_profile_event.dart';
part 'edit_profile_state.dart';

@injectable
class EditProfileBloc extends Bloc<EditProfileEvent, EditProfileState> {
  EditProfileBloc() : super(EditProfileInitial()) {
    on<SendEditProfileEvent>((event, emit) {});
    on<UploadImageToS3BucketEvent>((event, emit) async {
      await AwsS3.uploadFile(
        accessKey: "AKIAYW4JJT4MYTVMFNCT",
        secretKey: "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",
        bucket: "keringetan-avatar",
        region: "ap-southeast-2",
        file: event.image,
      ).then((value) {
        if (value != null) {
          emit(UploadAvatarSuccessState(value));
        } else {
          emit(const UploadAvatarFailed("Image url is not available"));
        }
      }).onError((error, stackTrace) {
        emit(UploadAvatarFailed(error.toString()));
      });
    });
  }
}
