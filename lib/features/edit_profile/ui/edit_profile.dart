// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:keringetan_consumer_flutter_app/core/deps/dependency_injection.dart';
import 'package:keringetan_consumer_flutter_app/core/extensions/context_ext.dart';
import 'package:keringetan_consumer_flutter_app/core/extensions/string_ext.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/image_picker.dart';
import 'package:keringetan_consumer_flutter_app/features/edit_profile/presentation/bloc/edit_profile_bloc.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';

import '../../../core/themes/app_styles.dart';

class EditProfilePage extends StatefulWidget {
  final UserModel model;

  const EditProfilePage({super.key, required this.model});

  static Widget builder(BuildContext context, UserModel model) {
    return BlocProvider<EditProfileBloc>(
      create: (context) {
        return getIt<EditProfileBloc>();
      },
      child: EditProfilePage(model: model),
    );
  }

  @override
  State<EditProfilePage> createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  final _nameController = TextEditingController();
  final _bioController = TextEditingController();
  var _selectedGender = "";
  var _uploadedAvatarUrl = "";

  @override
  void initState() {
    super.initState();
    _nameController.text = widget.model.name;
    _bioController.text = widget.model.bio;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          elevation: 0.5,
          leading: IconButton(
            splashRadius: 24,
            icon: const Icon(Icons.arrow_back_ios_new),
            onPressed: () {
              Navigator.pop(context, null);
            },
            color: AppColors.primary,
          ),
          actions: [
            IconButton(
              onPressed: () {},
              splashRadius: 24,
              color: AppColors.light,
              icon: const Icon(Icons.notifications),
            )
          ],
          title: Text(
            'Edit Profile',
            style: AppStyles.anekLatinBold.copyWith(
              fontSize: 16,
              color: AppColors.primary,
            ),
          ),
          backgroundColor: AppColors.light,
        ),
        body: SafeArea(
          maintainBottomViewPadding: true,
          child: BlocConsumer<EditProfileBloc, EditProfileState>(
            listener: (context, state) {
              if (state is UploadAvatarFailed) {
                context.showSnackBar(state.errorMessage);
              }
              if (state is EditProfileFailed) {
                context.showSnackBar(state.errorMessage);
              }
              if (state is UploadAvatarSuccessState) {
                _uploadedAvatarUrl = state.avatarUrl;
              }
            },
            builder: (context, state) {
              if (state is EditProfileInitial) {}
              return Container(
                width: double.infinity,
                color: AppColors.light,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    children: [
                      Stack(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 16,
                              right: 16,
                              bottom: 16,
                            ),
                            child: _handleAvatarState(state),
                          ),
                          Positioned(
                            bottom: 10,
                            right: 10,
                            child: Container(
                              width: 36,
                              height: 36,
                              decoration: BoxDecoration(
                                color: AppColors.primary,
                                shape: BoxShape.circle,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                      spreadRadius: 2,
                                      blurRadius: 5,
                                      offset: const Offset(
                                          0, 3) // changes position of shadow
                                      ),
                                ],
                              ),
                              child: InkWell(
                                onTap: () {
                                  _handleGallery();
                                },
                                child: Icon(
                                  size: 16,
                                  Icons.edit_outlined,
                                  color: AppColors.light,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                      const SizedBox(height: 16),
                      TextField(
                        controller: _nameController,
                        maxLines: 1,
                        maxLength: 24,
                        textCapitalization: TextCapitalization.sentences,
                        textInputAction: TextInputAction.go,
                        style:
                            AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                        cursorColor: AppColors.primary,
                        decoration: InputDecoration(
                          labelText: "Name",
                          labelStyle: AppStyles.anekLatinRegular.copyWith(
                            fontSize: 12,
                            color: AppColors.textHint,
                          ),
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          floatingLabelStyle:
                              AppStyles.anekLatinRegular.copyWith(
                            color: AppColors.primary,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.primary, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          isDense: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                        ),
                        enableSuggestions: false,
                        autocorrect: false,
                      ),
                      const SizedBox(height: 16),
                      TextField(
                        controller: _bioController,
                        textInputAction: TextInputAction.go,
                        style:
                            AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                        textCapitalization: TextCapitalization.sentences,
                        maxLines: 3,
                        minLines: 3,
                        maxLength: 144,
                        cursorColor: AppColors.primary,
                        decoration: InputDecoration(
                          labelText: "Write about yourself here..",
                          alignLabelWithHint: true,
                          labelStyle: AppStyles.anekLatinRegular.copyWith(
                            fontSize: 12,
                            color: AppColors.textHint,
                          ),
                          floatingLabelBehavior: FloatingLabelBehavior.auto,
                          floatingLabelStyle:
                              AppStyles.anekLatinRegular.copyWith(
                            color: AppColors.primary,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.primary, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          isDense: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                        ),
                        enableSuggestions: false,
                        autocorrect: false,
                      ),
                      const SizedBox(height: 16),
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Material(
                              color: Colors.transparent,
                              child: RadioListTile.adaptive(
                                  activeColor: AppColors.primary,
                                  toggleable: true,
                                  dense: true,
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                          color: AppColors.cardBorder),
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(8))),
                                  title: Text(
                                    "Male",
                                    style: AppStyles.anekLatinRegular
                                        .copyWith(fontSize: 12),
                                  ),
                                  value: "MALE",
                                  groupValue: _selectedGender,
                                  onChanged: (value) {
                                    setState(() {
                                      _selectedGender = value.orEmpty();
                                    });
                                  }),
                            ),
                          ),
                          const SizedBox(width: 16),
                          Expanded(
                            child: Material(
                              color: Colors.transparent,
                              child: RadioListTile.adaptive(
                                  activeColor: AppColors.primary,
                                  dense: true,
                                  toggleable: true,
                                  shape: RoundedRectangleBorder(
                                      side: BorderSide(
                                          color: AppColors.cardBorder),
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(8))),
                                  title: Text(
                                    "Female",
                                    style: AppStyles.anekLatinRegular
                                        .copyWith(fontSize: 12),
                                  ),
                                  value: "FEMALE",
                                  groupValue: _selectedGender,
                                  onChanged: (value) {
                                    setState(() {
                                      _selectedGender = value.orEmpty();
                                    });
                                  }),
                            ),
                          )
                        ],
                      ),
                      const Spacer(),
                      SizedBox(
                        width: double.infinity,
                        child: ElevatedButton.icon(
                          style: AppStyles.primaryButton,
                          onPressed: () {
                            _sendEditProfileEvent();
                          },
                          icon: _handleEditProfileIconState(state),
                          label: const Text("Save"),
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          ),
        ));
  }

  Widget _handleAvatarState(EditProfileState state) {
    if (state is UploadAvatarLoadingState) {
      return const Center(child: CircularProgressIndicator.adaptive());
    }

    return CachedNetworkImage(
      imageUrl:
          _uploadedAvatarUrl.isEmpty ? widget.model.avatar : _uploadedAvatarUrl,
      imageBuilder: (context, imageProvider) => Container(
        width: 96,
        height: 96,
        decoration: BoxDecoration(
          shape: BoxShape.rectangle,
          borderRadius: const BorderRadius.all(Radius.circular(16)),
          border: Border.all(
            width: 2,
            color: AppColors.light,
          ),
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.fill,
          ),
        ),
      ),
      placeholder: (context, url) => const CircularProgressIndicator.adaptive(),
      errorWidget: (context, url, error) => const Icon(Icons.face),
    );
  }

  void _sendEditProfileEvent() {
    context.read<EditProfileBloc>().add(
          SendEditProfileEvent(
            name: _nameController.text,
            bio: _bioController.text,
            gender: _selectedGender,
            avatar: '',
          ),
        );
  }

  Future<void> _handleGallery() async {
    var file = await ImagePickerUtils.open(
      ImageSource.gallery,
    );
    if (file != null) {
      context
          .read<EditProfileBloc>()
          .add(UploadImageToS3BucketEvent(image: file));
    }
  }

  Widget _handleEditProfileIconState(EditProfileState state) {
    if (state is EditProfileLoadingState) {
      return const SizedBox(
        height: 16,
        width: 16,
        child: CircularProgressIndicator.adaptive(),
      );
    }
    return const SizedBox.shrink();
  }
}
