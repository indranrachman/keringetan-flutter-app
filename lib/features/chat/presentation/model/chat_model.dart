import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';

class ChatModel {
  ChatModel({
    required this.messageId,
    required this.message,
    required this.timeStamp,
    required this.userModel,
  });
  final String messageId;
  final String message;
  final String timeStamp;
  final UserModel userModel;
}

class ChatInteractionModel {
  ChatInteractionModel({required this.userModel});
  final UserModel userModel;
}
