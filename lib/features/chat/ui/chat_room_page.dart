import 'package:flutter/material.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';

class ChatRoomPage extends StatefulWidget {
  const ChatRoomPage({super.key});

  @override
  State<ChatRoomPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatRoomPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0.5,
        centerTitle: false,
        title: Text(
          'Chat Room',
          style: AppStyles.anekLatinSemiBold.copyWith(
            fontSize: 16,
            color: AppColors.primary,
          ),
        ),
        backgroundColor: AppColors.light,
      ),
    );
  }
}
