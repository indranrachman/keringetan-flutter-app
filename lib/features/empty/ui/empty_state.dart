// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';

class EmptyState extends StatelessWidget {
  const EmptyState({
    super.key,
    this.onButtonClicked,
    required this.model,
  });

  final Function()? onButtonClicked;
  final EmptyStateModel model;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.light,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              height: 160,
              width: 160,
              "assets/images/img_empty_state.svg",
            ),
            const SizedBox(height: 8),
            Center(
              child: Text(
                model.title,
                style: AppStyles.anekLatinBold.copyWith(fontSize: 16),
              ),
            ),
            const SizedBox(height: 8),
            Center(
              child: Text(
                model.subTitle,
                style: AppStyles.anekLatinRegular,
              ),
            ),
            Visibility(
              visible: onButtonClicked != null,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  children: [
                    const SizedBox(height: 48),
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        style: AppStyles.primaryButton,
                        onPressed: () {
                          onButtonClicked?.call();
                        },
                        child: Text(
                          model.ctaText,
                          style: AppStyles.anekLatinBold.copyWith(
                            color: AppColors.light,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class EmptyStateModel {
  String title;
  String subTitle;
  String ctaText;
  EmptyStateModel({
    this.title = "",
    this.subTitle = "",
    this.ctaText = "",
  });
}
