// ignore_for_file: public_member_api_docs, sort_constructors_first

sealed class LoginState {}

class InitialLoginState extends LoginState {}

class LoginHideLoadingState extends LoginState {}

class LoginShowLoadingState extends LoginState {}

class LoginSuccessState extends LoginState {}

class LoginFailedState extends LoginState {
  final String errorMessage;
  LoginFailedState({
    this.errorMessage = '',
  });
}
