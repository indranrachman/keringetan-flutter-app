// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/features/login/data/login_repository.dart';
import 'package:keringetan_consumer_flutter_app/features/login/presentation/bloc/login_state.dart';

part 'login_event.dart';

@injectable
class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final LoginRepository repository;
  LoginBloc(
    this.repository,
  ) : super(InitialLoginState()) {
    on<SignInWithGoogleEvent>((event, emit) async {
      emit(LoginShowLoadingState());
      await repository
          .signInWithGoogle()
          .then((value) => emit(LoginSuccessState()))
          .onError((error, stackTrace) => emit(
                LoginFailedState(errorMessage: error.toString()),
              ))
          .whenComplete(() => emit(LoginHideLoadingState()));
    });
  }
}
