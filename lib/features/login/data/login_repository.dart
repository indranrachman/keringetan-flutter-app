// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/google/google_sign_in_provider.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/user/model/user_login_request.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/user/model/user_response.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/user/user_service.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/pref_utils.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';

abstract class LoginRepository {
  Future<UserResponse> signInWithGoogle();
}

@Injectable(as: LoginRepository)
class LoginRepositoryImpl implements LoginRepository {
  final GoogleSignInProvider googleSignInProvider;
  final UserService userService;
  final PrefUtils prefUtils;
  LoginRepositoryImpl({
    required this.googleSignInProvider,
    required this.userService,
    required this.prefUtils,
  });

  @override
  Future<UserResponse> signInWithGoogle() async {
    var response = await googleSignInProvider.signIn();
    if (response.user != null) {
      var request = UserLoginRequest(
        email: response.user?.email ?? "",
        name: response.user?.displayName ?? "",
      );
      var userResponse = await userService.signIn(request);
      var model = UserModel(
        id: userResponse.data.id,
        email: userResponse.data.email,
        name: userResponse.data.name,
        avatar: userResponse.data.avatar,
        bio: userResponse.data.bio,
        gender: userResponse.data.gender,
      );
      prefUtils.setUser(model.toJson());
      prefUtils.setUserId(model.id);
      return userResponse.data;
    }
    return Future.error(Exception("User not found"));
  }
}
