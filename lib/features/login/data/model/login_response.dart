// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:keringetan_consumer_flutter_app/core/apis/user/model/user_response.dart';

class LoginResponse {
  final UserResponse data;
  LoginResponse({
    required this.data,
  });

  LoginResponse copyWith({
    UserResponse? data,
  }) {
    return LoginResponse(
      data: data ?? this.data,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'data': data.toMap(),
    };
  }

  factory LoginResponse.fromMap(Map<String, dynamic> map) {
    return LoginResponse(
      data: UserResponse.fromMap(map['data'] as Map<String, dynamic>),
    );
  }

  String toJson() => json.encode(toMap());

  factory LoginResponse.fromJson(String source) =>
      LoginResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'LoginResponse(data: $data)';

  @override
  bool operator ==(covariant LoginResponse other) {
    if (identical(this, other)) return true;

    return other.data == data;
  }

  @override
  int get hashCode => data.hashCode;
}
