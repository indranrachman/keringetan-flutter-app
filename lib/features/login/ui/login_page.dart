import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:keringetan_consumer_flutter_app/core/deps/dependency_injection.dart';
import 'package:keringetan_consumer_flutter_app/core/extensions/context_ext.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';
import 'package:keringetan_consumer_flutter_app/features/login/presentation/bloc/login_bloc.dart';
import 'package:keringetan_consumer_flutter_app/features/login/presentation/bloc/login_state.dart';
import 'package:keringetan_consumer_flutter_app/features/signup/ui/sign_up_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  static Widget builder(BuildContext context) {
    return BlocProvider<LoginBloc>(
      create: (context) => getIt<LoginBloc>(),
      child: const LoginPage(),
    );
  }

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late LoginBloc bloc;
  // final _emailController = TextEditingController();
  // final _passwordController = TextEditingController();
  // var _isEmailFilled = false;
  // var _isPasswordFilled = false;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<LoginBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primary,
      resizeToAvoidBottomInset: true,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.close_outlined),
          onPressed: () {
            Navigator.pop(context, null);
          },
          color: AppColors.light,
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: AppColors.primary,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          color: AppColors.primary,
          child: SizedBox(
            width: double.infinity,
            child: SafeArea(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        "assets/images/img_login.svg",
                        height: 128,
                        width: 128,
                      ),
                      const SizedBox(height: 16),
                      Text(
                        'Hi there!',
                        textAlign: TextAlign.center,
                        style: AppStyles.anekLatinSemiBold.copyWith(
                          fontSize: 36,
                          color: AppColors.light,
                        ),
                      ),
                      const SizedBox(height: 8),
                      Text(
                        'You need to sign in so we can call you by your name.\nWe promise it\'s gonna be easy like sunday morning~',
                        textAlign: TextAlign.center,
                        style: AppStyles.anekLatinRegular
                            .copyWith(color: AppColors.light, fontSize: 12),
                      ),
                      const SizedBox(height: 24),
                      // TextField(
                      //   onChanged: (value) {
                      //     if (value.isNotEmpty && !_isEmailFilled) {
                      //       setState(() {
                      //         _isEmailFilled = true;
                      //       });
                      //     }
                      //     if (value.isEmpty && _isEmailFilled) {
                      //       setState(() {
                      //         _isEmailFilled = false;
                      //       });
                      //     }
                      //   },
                      //   controller: _emailController,
                      //   maxLines: 1,
                      //   keyboardType: TextInputType.emailAddress,
                      //   style:
                      //       AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                      //   cursorColor: AppColors.primary,
                      //   decoration: InputDecoration(
                      //     fillColor: AppColors.light,
                      //     filled: true,
                      //     prefixIcon: const Icon(Icons.email),
                      //     prefixIconColor: AppColors.primary,
                      //     suffixIcon: Visibility(
                      //       visible: _isEmailFilled,
                      //       child: GestureDetector(
                      //         child: Icon(
                      //           Icons.close,
                      //           color: AppColors.primary,
                      //         ),
                      //         onTap: () {
                      //           setState(() {
                      //             _isEmailFilled = false;
                      //           });
                      //           _emailController.clear();
                      //         },
                      //       ),
                      //     ),
                      //     labelText: "johndoe@xyz.com",
                      //     labelStyle: AppStyles.anekLatinRegular.copyWith(
                      //       fontSize: 12,
                      //       color: AppColors.textHint,
                      //     ),
                      //     floatingLabelBehavior: FloatingLabelBehavior.never,
                      //     floatingLabelStyle:
                      //         AppStyles.anekLatinRegular.copyWith(
                      //       color: AppColors.primary,
                      //     ),
                      //     focusedBorder: OutlineInputBorder(
                      //       borderSide:
                      //           BorderSide(color: AppColors.light, width: 1),
                      //       borderRadius:
                      //           const BorderRadius.all(Radius.circular(8)),
                      //     ),
                      //     isDense: true,
                      //     enabledBorder: OutlineInputBorder(
                      //       borderSide:
                      //           BorderSide(color: AppColors.light, width: 1),
                      //       borderRadius:
                      //           const BorderRadius.all(Radius.circular(8)),
                      //     ),
                      //     border: OutlineInputBorder(
                      //       borderSide:
                      //           BorderSide(color: AppColors.light, width: 1),
                      //       borderRadius:
                      //           const BorderRadius.all(Radius.circular(8)),
                      //     ),
                      //   ),
                      //   enableSuggestions: false,
                      // ),
                      // const SizedBox(height: 16),
                      // TextField(
                      //   onChanged: (value) {
                      //     if (value.isNotEmpty && !_isPasswordFilled) {
                      //       setState(() {
                      //         _isPasswordFilled = true;
                      //       });
                      //     }
                      //     if (value.isEmpty && _isPasswordFilled) {
                      //       setState(() {
                      //         _isPasswordFilled = false;
                      //       });
                      //     }
                      //   },
                      //   controller: _passwordController,
                      //   keyboardType: TextInputType.visiblePassword,
                      //   maxLines: 1,
                      //   style:
                      //       AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                      //   cursorColor: AppColors.primary,
                      //   decoration: InputDecoration(
                      //     fillColor: AppColors.light,
                      //     filled: true,
                      //     prefixIcon: const Icon(Icons.lock),
                      //     prefixIconColor: AppColors.primary,
                      //     suffixIcon: Visibility(
                      //       visible: _isPasswordFilled,
                      //       child: GestureDetector(
                      //         child: Icon(
                      //           Icons.close,
                      //           color: AppColors.primary,
                      //         ),
                      //         onTap: () {
                      //           setState(() {
                      //             _isPasswordFilled = false;
                      //           });
                      //           _passwordController.clear();
                      //         },
                      //       ),
                      //     ),
                      //     labelText: "Password min 8 characters",
                      //     labelStyle: AppStyles.anekLatinRegular.copyWith(
                      //       fontSize: 12,
                      //       color: AppColors.textHint,
                      //     ),
                      //     floatingLabelBehavior: FloatingLabelBehavior.never,
                      //     floatingLabelStyle:
                      //         AppStyles.anekLatinRegular.copyWith(
                      //       color: AppColors.primary,
                      //     ),
                      //     focusedBorder: OutlineInputBorder(
                      //       borderSide:
                      //           BorderSide(color: AppColors.light, width: 1),
                      //       borderRadius:
                      //           const BorderRadius.all(Radius.circular(8)),
                      //     ),
                      //     isDense: true,
                      //     enabledBorder: OutlineInputBorder(
                      //       borderSide:
                      //           BorderSide(color: AppColors.light, width: 1),
                      //       borderRadius:
                      //           const BorderRadius.all(Radius.circular(8)),
                      //     ),
                      //     border: OutlineInputBorder(
                      //       borderSide:
                      //           BorderSide(color: AppColors.light, width: 1),
                      //       borderRadius:
                      //           const BorderRadius.all(Radius.circular(8)),
                      //     ),
                      //   ),
                      //   enableSuggestions: false,
                      // ),
                      // const SizedBox(height: 16),
                      // BlocConsumer<LoginBloc, LoginState>(
                      //   listener: (context, state) {
                      //     if (state.errorMessage.isNotEmpty) {
                      //       context.showSnackBar(state.errorMessage);
                      //     }
                      //     if (state.isGoogleSignInSuccess) {
                      //       Navigator.pop(context);
                      //     }
                      //   },
                      //   builder: (context, state) {
                      //     return SizedBox(
                      //       width: double.infinity,
                      //       child: ElevatedButton.icon(
                      //         style: AppStyles.primaryButtonInverted,
                      //         onPressed: () {
                      //           _sendSignInEvent();
                      //         },
                      //         icon: state.isGoogleSignInLoading
                      //             ? Container(
                      //                 width: 20,
                      //                 height: 20,
                      //                 padding: const EdgeInsets.all(2.0),
                      //                 child: CircularProgressIndicator(
                      //                   color: AppColors.primary,
                      //                   strokeWidth: 3,
                      //                 ),
                      //               )
                      //             : const SizedBox.shrink(),
                      //         label: Text(
                      //           state.isGoogleSignInLoading ? "" : "Sign in",
                      //           style: AppStyles.anekLatinBold.copyWith(
                      //             color: AppColors.primary,
                      //             fontSize: 12,
                      //           ),
                      //         ),
                      //       ),
                      //     );
                      //   },
                      // ),
                      // const SizedBox(height: 16),
                      // Row(
                      //   children: [
                      //     Expanded(
                      //         child: Divider(
                      //       height: 2,
                      //       color: AppColors.light,
                      //     )),
                      //     Padding(
                      //       padding: const EdgeInsets.only(
                      //         left: 8.0,
                      //         right: 8.0,
                      //       ),
                      //       child: Text(
                      //         "Or",
                      //         style: AppStyles.anekLatinSemiBold.copyWith(
                      //           color: AppColors.light,
                      //         ),
                      //       ),
                      //     ),
                      //     Expanded(
                      //         child: Divider(
                      //       height: 2,
                      //       color: AppColors.light,
                      //     )),
                      //   ],
                      // ),
                      // const SizedBox(height: 12),
                      BlocConsumer<LoginBloc, LoginState>(
                        listener: (context, state) {
                          if (state is LoginFailedState) {
                            context.showSnackBar(state.errorMessage);
                          }
                          if (state is LoginSuccessState) {
                            Navigator.pop(context, true);
                          }
                        },
                        builder: (context, state) {
                          return SizedBox(
                            width: double.infinity,
                            child: ElevatedButton.icon(
                              style: AppStyles.primaryButtonInverted,
                              onPressed: () {
                                bloc.add(SignInWithGoogleEvent());
                              },
                              icon: _handleLoginButtonIcon(state),
                              label: Text(
                                _handleLoginButtonText(state),
                                style: AppStyles.anekLatinBold.copyWith(
                                  color: AppColors.primary,
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                  Visibility(
                    visible: false,
                    child: Positioned(
                      bottom: 0,
                      child: Center(
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                              text: "Don't have an account?",
                              style: AppStyles.anekLatinRegular.copyWith(
                                color: AppColors.light,
                                fontSize: 12,
                              ),
                              children: [
                                TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        Navigator.of(context,
                                                rootNavigator: true)
                                            .push(MaterialPageRoute(
                                          builder: (context) {
                                            return const SignUpPage();
                                          },
                                        ));
                                      },
                                    text: " Sign Up",
                                    style: AppStyles.anekLatinBold.copyWith(
                                      color: AppColors.accent,
                                      fontSize: 12,
                                    ))
                              ]),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  String _handleLoginButtonText(LoginState state) {
    return state is LoginShowLoadingState ? "" : "Sign in with Google";
  }

  StatelessWidget _handleLoginButtonIcon(LoginState state) {
    if (state is LoginShowLoadingState) {
      return Container(
        width: 20,
        height: 20,
        padding: const EdgeInsets.all(2.0),
        child: CircularProgressIndicator(
          color: AppColors.primary,
          strokeWidth: 3,
        ),
      );
    } else {
      return SvgPicture.asset(
        "assets/icons/ic_google.svg",
        width: 16,
        height: 16,
      );
    }
  }

  // void _sendSignInEvent() {
  //   bloc.add(
  //     SignInEvent(
  //       email: _emailController.text,
  //       password: _passwordController.text,
  //     ),
  //   );
  // }
}
