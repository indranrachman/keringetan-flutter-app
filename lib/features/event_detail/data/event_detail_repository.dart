import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';

abstract class EventDetailRepository {
  Future<EventModel> get();
}
