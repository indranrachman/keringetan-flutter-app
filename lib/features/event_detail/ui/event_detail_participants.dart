// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';

class EventDetailParticipant extends StatelessWidget {
  const EventDetailParticipant({
    Key? key,
    required this.model,
  }) : super(key: key);

  final EventModel model;

  //var _isExpanded = true;

  @override
  Widget build(BuildContext context) {
    return _renderHeader();
    // if (widget.model.participants.isEmpty) {
    //   return _renderHeader();
    // } else {
    //   return _renderCollapsibleParticipant();
    // }
  }

  // Widget _renderCollapsibleParticipant() {
  Widget _renderHeader() {
    return Row(
      children: [
        Container(
          width: 48,
          height: 48,
          decoration: BoxDecoration(
            color: AppColors.pillMutedBackground,
            shape: BoxShape.rectangle,
            borderRadius: const BorderRadius.all(
              Radius.circular(8),
            ),
          ),
          child: Icon(
            Icons.group,
            color: AppColors.primary,
          ),
        ),
        const SizedBox(width: 8),
        Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "${model.participants.length} Participants",
              style: AppStyles.anekLatinSemiBold,
            ),
            const SizedBox(width: 4),
            Text(
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              _handleParticipantsText,
              style: AppStyles.anekLatinRegular.copyWith(
                fontSize: 12,
                color: AppColors.textCaption,
              ),
            ),
          ],
        ),
        const Spacer(),
        Visibility(
          visible: false,
          child: Material(
            child: InkWell(
              child: Container(
                width: 36,
                height: 36,
                decoration: BoxDecoration(
                  color: AppColors.primary,
                  shape: BoxShape.circle,
                ),
                child: Icon(
                  size: 16,
                  Icons.comment_outlined,
                  color: AppColors.light,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  bool get _getChatVisibility =>
      model.participants.isNotEmpty && model.state != "OPEN";

  String get _handleParticipantsText {
    return "No one jas joined yet :(";
    // switch (model.participants.length) {
    //   case 0:
    //     return "No one jas joined yet :(";
    //   case 1:
    //     return "Joined by ${model.participants[0].name}";
    //   default:
    //     return "${model.participants[0].name}, ${model.participants[1].name} and others";
    // }
  }
}
