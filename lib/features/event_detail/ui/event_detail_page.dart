import 'package:flutter/material.dart';
import 'package:keringetan_consumer_flutter_app/core/constants/event_status.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';
import 'package:keringetan_consumer_flutter_app/features/edit_event/ui/edit_event_page.dart';
import 'package:keringetan_consumer_flutter_app/features/event_detail/ui/event_detail_participants.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';
import 'package:keringetan_consumer_flutter_app/features/login/ui/login_page.dart';

class EventDetailPage extends StatefulWidget {
  final EventModel model;
  const EventDetailPage({
    Key? key,
    required this.model,
  }) : super(key: key);

  @override
  State<EventDetailPage> createState() => _EventDetailPageState();
}

class _EventDetailPageState extends State<EventDetailPage> {
  var _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.light,
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 0,
            bottom: 12,
            left: 16,
            right: 16,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Stack(
                children: [
                  Row(
                    children: [
                      Container(
                        width: 48,
                        height: 48,
                        decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8)),
                          image: DecorationImage(
                            image: NetworkImage(widget.model.poster.avatar),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      const SizedBox(width: 8),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            widget.model.poster.name,
                            style: AppStyles.anekLatinBold,
                          ),
                          const SizedBox(height: 2),
                          Text(
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            widget.model.category.name,
                            style: AppStyles.anekLatinSemiBold.copyWith(
                              fontSize: 12,
                              color: AppColors.textCaption,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Positioned(
                    top: 0,
                    right: 0,
                    child: Text(
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      widget.model.timeStamp,
                      style: AppStyles.anekLatinRegular.copyWith(
                        fontSize: 12,
                        color: AppColors.textHint,
                      ),
                    ),
                  ),
                  // Positioned(
                  //   top: 0,
                  //   right: 0,
                  //   child: EventStatusBadge(
                  //     model: widget.model,
                  //   ),
                  // ),
                ],
              ),
              const SizedBox(height: 12),
              Container(
                width: double.infinity,
                height: 160,
                decoration: const BoxDecoration(
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.all(Radius.circular(8)),
                  image: DecorationImage(
                    image: NetworkImage(
                        'https://lh3.googleusercontent.com/p/AF1QipNtxJS-M4q1x-ng1QlDoqE_WZusPY2lk8ONLD6k=s1360-w1360-h1020'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Visibility(
                visible: widget.model.caption.isNotEmpty,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 12),
                    Text(
                      widget.model.caption,
                      style: AppStyles.anekLatinRegular.copyWith(
                        fontSize: 12,
                        color: AppColors.textCaption,
                      ),
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 12),
              Divider(
                height: 2,
                color: AppColors.viewDivider,
              ),
              const SizedBox(height: 12),
              Column(
                children: [
                  EventDetailParticipant(model: widget.model),
                  const SizedBox(height: 16),
                  Row(
                    children: [
                      Container(
                        width: 48,
                        height: 48,
                        decoration: BoxDecoration(
                          color: AppColors.pillMutedBackground,
                          shape: BoxShape.rectangle,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        child: Icon(
                          Icons.pin_drop,
                          color: AppColors.primary,
                        ),
                      ),
                      const SizedBox(width: 8),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.model.location.name,
                            style: AppStyles.anekLatinSemiBold,
                          ),
                          const SizedBox(height: 4),
                          Text(
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            widget.model.meetingNotes,
                            style: AppStyles.anekLatinRegular.copyWith(
                              fontSize: 12,
                              color: AppColors.textCaption,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(height: 16),
                  Row(
                    children: [
                      Container(
                        width: 48,
                        height: 48,
                        decoration: BoxDecoration(
                          color: AppColors.pillMutedBackground,
                          shape: BoxShape.rectangle,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        child: Icon(
                          Icons.date_range,
                          color: AppColors.primary,
                        ),
                      ),
                      const SizedBox(width: 8),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Text(
                                widget.model.date,
                                style: AppStyles.anekLatinSemiBold,
                              ),
                            ],
                          ),
                          const SizedBox(height: 4),
                          Row(
                            children: [
                              Text(
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                widget.model.time,
                                style: AppStyles.anekLatinRegular.copyWith(
                                  fontSize: 12,
                                  color: AppColors.textCaption,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              const SizedBox(height: 16),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton.icon(
                  style: AppStyles.primaryButton,
                  icon: AnimatedSwitcher(
                    transitionBuilder: (child, animation) {
                      return ScaleTransition(
                        scale: animation,
                        child: child,
                      );
                    },
                    duration: const Duration(milliseconds: 300),
                    child: _setIcon(),
                  ),
                  onPressed: _handleButtonRedirection(),
                  label: Text(
                    _handleButtonText(),
                    style: AppStyles.anekLatinBold.copyWith(
                      color: AppColors.light,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  String _handleButtonText() {
    if (_isLoading) {
      return "";
    }
    var eventStatus = EventStatusUtils.fromString(widget.model.state);
    switch (eventStatus) {
      case EventStatus.open:
        return "Join";
      case EventStatus.joined:
        return "Leave";
      case EventStatus.byYou:
        return "Edit";
      default:
        return "";
    }
  }

  _handleButtonRedirection() {
    if (_isLoading) {
      return null;
    }
    var eventStatus = EventStatusUtils.fromString(widget.model.state);
    switch (eventStatus) {
      case EventStatus.open:
        return _handleJoinRequest;
      case EventStatus.joined:
        return _handleLeaveEventRequest;
      case EventStatus.byYou:
        return _handleEditEventRequest;
      default:
        return null;
    }
  }

  Widget? _setIcon() {
    if (_isLoading) {
      return Container(
        width: 20,
        height: 20,
        padding: const EdgeInsets.all(2.0),
        child: const CircularProgressIndicator(
          color: Colors.white,
          strokeWidth: 3,
        ),
      );
    } else {
      return null;
    }
  }

  void _handleEditEventRequest() {
    Navigator.of(context, rootNavigator: true)
        .push(
      MaterialPageRoute(
        fullscreenDialog: true,
        builder: (context) {
          return EditEventPage.builder(
            context,
            widget.model,
          );
        },
      ),
    )
        .then((value) {
      if (value != null) {
        Navigator.pop(context, value);
      }
    });
  }

  void _handleLeaveEventRequest() {
    setState(() => _isLoading = true);
    Future.delayed(
      const Duration(seconds: 2),
      () => setState(() {
        Navigator.pop(context, "You're now leave the event..");
      }),
    );
  }

  void _handleJoinRequest() {
    Navigator.pop(context);
    Navigator.of(
      context,
      rootNavigator: true,
    ).push(MaterialPageRoute(
      builder: (context) {
        return LoginPage.builder(context);
      },
      fullscreenDialog: true,
    ));
    // setState(() => _isLoading = true);
    // Future.delayed(
    //   const Duration(seconds: 2),
    //   () => setState(() {
    //     Navigator.pop(context, "Congrats, you're now join the event!");
    //   }),
    // );
  }
}
