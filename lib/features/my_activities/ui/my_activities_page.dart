import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:keringetan_consumer_flutter_app/features/empty/ui/empty_state.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';

class MyActivitiesPage extends StatelessWidget {
  static Widget builder(BuildContext context) {
    return const MyActivitiesPage();
  }

  const MyActivitiesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: false,
          elevation: 2,
          actions: [
            IconButton(
              onPressed: () {},
              color: AppColors.light,
              icon: const Icon(Icons.notifications),
            )
          ],
          title: Text(
            'Keringetan',
            style: GoogleFonts.poppins().copyWith(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              color: AppColors.light,
            ),
          ),
          backgroundColor: AppColors.primary,
        ),
        body: EmptyState(
          model: EmptyStateModel(),
        ));
  }
}
