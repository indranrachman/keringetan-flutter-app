// ignore_for_file: public_member_api_docs, sort_constructors_first

import 'package:equatable/equatable.dart';

class CreateEventState extends Equatable {
  final bool? isSuccess;
  final bool? isLoading;
  final String? error;
  const CreateEventState({
    this.isSuccess = false,
    this.isLoading = false,
    this.error = '',
  });

  CreateEventState copyWith({
    bool? isSucess,
    bool? isLoading,
    String? error,
  }) {
    return CreateEventState(
      isSuccess: isSucess,
      isLoading: isLoading,
      error: error,
    );
  }

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [isSuccess, isLoading, error];
}
