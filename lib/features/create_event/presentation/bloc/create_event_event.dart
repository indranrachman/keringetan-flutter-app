// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'create_event_bloc.dart';

abstract class CreateEventEvent extends Equatable {
  const CreateEventEvent();

  @override
  List<Object> get props => [];
}

class SendCreateEvent extends CreateEventEvent {
  final int category;
  final String caption;
  final String meetingNotes;
  final String date;
  final String locationName;
  final double locationLatitude;
  final double locationLongitude;
  const SendCreateEvent({
    this.category = 0,
    this.caption = '',
    this.meetingNotes = '',
    this.date = '',
    this.locationName = '',
    this.locationLatitude = 0.0,
    this.locationLongitude = 0.0,
  });
}
