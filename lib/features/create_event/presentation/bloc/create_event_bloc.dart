// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/model/request/create_event_request.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/pref_utils.dart';

import 'package:keringetan_consumer_flutter_app/features/create_event/data/create_event_repository.dart';
import 'package:keringetan_consumer_flutter_app/features/create_event/presentation/bloc/create_event_state.dart';

part 'create_event_event.dart';

@injectable
class CreateEventBloc extends Bloc<CreateEventEvent, CreateEventState> {
  final CreateEventRepository repository;
  final PrefUtils prefUtils;

  CreateEventBloc(
    this.repository,
    this.prefUtils,
  ) : super(const CreateEventState()) {
    on<SendCreateEvent>((event, emit) async {
      var request = CreateEventRequest(
        posterId: prefUtils.getUserId() ?? -1,
        categoryId: event.category,
        caption: event.caption,
        meetingNotes: event.meetingNotes,
        date: event.date,
        location: CreateEventLocationRequest(
          name: event.locationName,
          latitude: event.locationLatitude,
          longitude: event.locationLongitude,
        ),
      );
      emit(state.copyWith(isLoading: true));
      await repository
          .createEvent(request)
          .then(
            (response) => emit(state.copyWith(
              isLoading: false,
              isSucess: response != null,
            )),
          )
          .catchError(
        (error, stackTrace) {
          _handleError(emit, stackTrace);
        },
      );
    });
  }

  void _handleError(
    Emitter<CreateEventState> emit,
    StackTrace stackTrace,
  ) {
    emit(
      state.copyWith(
        isLoading: false,
        error: stackTrace.toString(),
      ),
    );
  }
}
