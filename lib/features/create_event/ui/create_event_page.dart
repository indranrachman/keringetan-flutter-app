import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:keringetan_consumer_flutter_app/core/deps/dependency_injection.dart';
import 'package:keringetan_consumer_flutter_app/core/extensions/context_ext.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/data_faker.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/date_time_utils.dart';
import 'package:keringetan_consumer_flutter_app/features/create_event/presentation/bloc/create_event_bloc.dart';
import 'package:keringetan_consumer_flutter_app/features/create_event/presentation/bloc/create_event_state.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';
import 'package:keringetan_consumer_flutter_app/features/location_picker/ui/location_picker.dart';
import 'package:keringetan_consumer_flutter_app/widgets/create_event_category_item.dart';

class CreateEventPage extends StatefulWidget {
  const CreateEventPage({super.key});

  static Widget builder(BuildContext context) {
    return BlocProvider<CreateEventBloc>(
      create: (context) => getIt<CreateEventBloc>(),
      child: const CreateEventPage(),
    );
  }

  @override
  State<CreateEventPage> createState() => _CreateEventPageState();
}

class _CreateEventPageState extends State<CreateEventPage> {
  final _captionController = TextEditingController();
  final _meetingNotesController = TextEditingController();
  final _dateController = TextEditingController();
  final _timeController = TextEditingController();
  final _locationController = TextEditingController();

  var _categoryItems = DataFaker.categories;

  TimeOfDay? _selectedTime;
  DateTime? _selectedDate;
  double locationLat = 0.0;
  double locationLng = 0.0;

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<CreateEventBloc, CreateEventState>(
      listener: (context, state) {
        if (state.error != null) {
          context.showSnackBar(state.error);
        }
        if (state.isSuccess == true) {
          Navigator.pop(context, "Your event has been posted!");
        }
      },
      builder: (context, state) {
        return Scaffold(
          backgroundColor: AppColors.light,
          resizeToAvoidBottomInset: true,
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            leading: IconButton(
              splashRadius: 24,
              icon: const Icon(Icons.close_outlined),
              onPressed: () {
                Navigator.pop(context, null);
              },
              color: AppColors.primary,
            ),
            centerTitle: true,
            elevation: 0.5,
            title: Text(
              'Create Event',
              style: AppStyles.anekLatinBold.copyWith(
                color: AppColors.primary,
                fontSize: 16,
              ),
            ),
            backgroundColor: AppColors.light,
          ),
          body: SafeArea(
            maintainBottomViewPadding: true,
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: SingleChildScrollView(
                clipBehavior: Clip.none,
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Choose Category',
                        style: AppStyles.anekLatinSemiBold,
                      ),
                    ),
                    const SizedBox(height: 16),
                    GridView.builder(
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate:
                          const SliverGridDelegateWithMaxCrossAxisExtent(
                        maxCrossAxisExtent: 180,
                        childAspectRatio: 2.2,
                        crossAxisSpacing: 12,
                        mainAxisSpacing: 12,
                      ),
                      itemCount: _categoryItems.length,
                      itemBuilder: (BuildContext ctx, index) {
                        return CategoryEventItem(
                          onCategoryItemClicked: (id) {
                            setState(() {
                              _categoryItems = _categoryItems
                                  .map(
                                    (category) => CategoryModel(
                                      id: category.id,
                                      name: category.name,
                                      isSelected: category.id == id,
                                    ),
                                  )
                                  .toList();
                            });
                          },
                          model: CategoryModel(
                            id: _categoryItems[index].id,
                            name: _categoryItems[index].name,
                            isSelected: _categoryItems[index].isSelected,
                          ),
                        );
                      },
                    ),
                    const SizedBox(height: 12),
                    TextField(
                      textInputAction: TextInputAction.go,
                      style: AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                      textCapitalization: TextCapitalization.sentences,
                      maxLines: 3,
                      minLines: 3,
                      maxLength: 144,
                      controller: _captionController,
                      cursorColor: AppColors.primary,
                      decoration: InputDecoration(
                        labelText: "Write your best caption here..",
                        alignLabelWithHint: true,
                        labelStyle: AppStyles.anekLatinRegular.copyWith(
                          fontSize: 12,
                          color: AppColors.textHint,
                        ),
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        floatingLabelStyle: AppStyles.anekLatinRegular.copyWith(
                          color: AppColors.primary,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: AppColors.primary, width: 1),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8)),
                        ),
                        isDense: true,
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: AppColors.cardBorder, width: 1),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8)),
                        ),
                        border: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: AppColors.cardBorder, width: 1),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8)),
                        ),
                      ),
                      enableSuggestions: false,
                      autocorrect: false,
                    ),
                    const SizedBox(height: 4),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Location Details',
                        style: AppStyles.anekLatinSemiBold,
                      ),
                    ),
                    const SizedBox(height: 16),
                    InkWell(
                      onTap: () {
                        showModalBottomSheet(
                          barrierColor: AppColors.bottomSheetBarrierColor,
                          backgroundColor: AppColors.light,
                          showDragHandle: true,
                          context: context,
                          isScrollControlled: true,
                          useSafeArea: true,
                          useRootNavigator: true,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.vertical(
                              top: Radius.circular(20.0),
                            ),
                          ),
                          builder: (context) {
                            return SizedBox(
                              height: MediaQuery.of(context).size.height,
                              child: LocationPickerPage.builder(
                                context,
                                (model) {
                                  _locationController.text = model.name;
                                  locationLat = model.latitude;
                                  locationLng = model.longitude;
                                },
                              ),
                            );
                          },
                        );
                      },
                      child: TextField(
                        maxLines: 1,
                        enabled: false,
                        controller: _locationController,
                        style:
                            AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                        cursorColor: AppColors.primary,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(
                            Icons.pin_drop_rounded,
                          ),
                          prefixIconColor: AppColors.primary,
                          labelText: "Input location here",
                          labelStyle: AppStyles.anekLatinRegular.copyWith(
                            fontSize: 12,
                            color: AppColors.textHint,
                          ),
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          floatingLabelStyle:
                              AppStyles.anekLatinRegular.copyWith(
                            color: AppColors.primary,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.primary, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          isDense: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                        ),
                        enableSuggestions: false,
                      ),
                    ),
                    const SizedBox(height: 16),
                    TextField(
                      maxLines: 1,
                      textCapitalization: TextCapitalization.sentences,
                      textInputAction: TextInputAction.go,
                      controller: _meetingNotesController,
                      style: AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                      cursorColor: AppColors.primary,
                      decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.note_alt_outlined),
                        prefixIconColor: AppColors.primary,
                        labelText: "Meeting point(eg:Parkiran Fasilkom UI)",
                        labelStyle: AppStyles.anekLatinRegular.copyWith(
                          fontSize: 12,
                          color: AppColors.textHint,
                        ),
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        floatingLabelStyle: AppStyles.anekLatinRegular.copyWith(
                          color: AppColors.primary,
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: AppColors.primary, width: 1),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8)),
                        ),
                        isDense: true,
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: AppColors.cardBorder, width: 1),
                          borderRadius:
                              const BorderRadius.all(Radius.circular(8)),
                        ),
                      ),
                      enableSuggestions: false,
                      autocorrect: false,
                    ),
                    const SizedBox(height: 16),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Time Details',
                        style: AppStyles.anekLatinSemiBold,
                      ),
                    ),
                    const SizedBox(height: 16),
                    InkWell(
                      onTap: () {
                        _showDatePicker(context);
                      },
                      child: TextField(
                        maxLines: 1,
                        enabled: false,
                        controller: _dateController,
                        style:
                            AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                        cursorColor: AppColors.primary,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.date_range),
                          prefixIconColor: AppColors.primary,
                          labelText: "Choose schedule upto 7 days from now",
                          labelStyle: AppStyles.anekLatinRegular.copyWith(
                            fontSize: 12,
                            color: AppColors.textHint,
                          ),
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          floatingLabelStyle:
                              AppStyles.anekLatinRegular.copyWith(
                            color: AppColors.primary,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.primary, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          isDense: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                        ),
                        enableSuggestions: false,
                      ),
                    ),
                    const SizedBox(height: 16),
                    InkWell(
                      onTap: () {
                        _showTimePicker(context);
                      },
                      child: TextField(
                        enabled: false,
                        maxLines: 1,
                        controller: _timeController,
                        style:
                            AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                        cursorColor: AppColors.primary,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.watch),
                          prefixIconColor: AppColors.primary,
                          labelText: "When the event start?",
                          labelStyle: AppStyles.anekLatinRegular.copyWith(
                            fontSize: 12,
                            color: AppColors.textHint,
                          ),
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          floatingLabelStyle:
                              AppStyles.anekLatinRegular.copyWith(
                            color: AppColors.primary,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.primary, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          isDense: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                        ),
                        enableSuggestions: false,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          bottomNavigationBar: Container(
            decoration: BoxDecoration(
              color: AppColors.light,
              border: Border(
                  top: BorderSide(
                color: AppColors.cardBorder,
                width: 1,
              )),
            ),
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 16,
                  right: 16,
                  top: 12,
                  bottom: 12,
                ),
                child: SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                    style: AppStyles.primaryButton,
                    onPressed: state.isLoading == true
                        ? null
                        : () {
                            _handleCreateEvent(context);
                          },
                    child: _handleTextLoading(state),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _handleTextLoading(CreateEventState state) {
    if (state.isLoading == true) {
      return Container(
        width: 20,
        height: 20,
        padding: const EdgeInsets.all(2.0),
        child: const CircularProgressIndicator(
          color: Colors.white,
          strokeWidth: 3,
        ),
      );
    } else {
      return Text(
        "Create",
        style: AppStyles.anekLatinBold.copyWith(
          color: AppColors.light,
        ),
      );
    }
  }

  void _handleCreateEvent(BuildContext context) {
    context.read<CreateEventBloc>().add(
          SendCreateEvent(
            category:
                _categoryItems.firstWhere((element) => element.isSelected).id,
            caption: _captionController.text,
            locationName: _locationController.text,
            locationLatitude: locationLat,
            locationLongitude: locationLng,
            meetingNotes: _meetingNotesController.text,
            date: DateTime.parse(_selectedDate.toString())
                .format('yyyy-MM-dd HH:mm'),
          ),
        );
  }

  Future<void> _showTimePicker(BuildContext context) async {
    var selectedTime = await showTimePicker(
      context: context,
      initialEntryMode: TimePickerEntryMode.inputOnly,
      initialTime: _determineInitialTime(),
      builder: (context, child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
          child: child ?? Container(),
        );
      },
    );
    if (selectedTime != null) {
      _selectedDate = _selectedDate?.copyWith(
        hour: selectedTime.hour,
        minute: selectedTime.minute,
      );
      _selectedTime = selectedTime;
      _timeController.text = selectedTime.to24hours();
    }
  }

  TimeOfDay _determineInitialTime() {
    return (_selectedTime != null) ? _selectedTime! : TimeOfDay.now();
  }

  Future<void> _showDatePicker(BuildContext context) async {
    var selectedDate = await showDatePicker(
        context: context,
        builder: (context, child) {
          return Theme(
            data: Theme.of(context).copyWith(
              colorScheme: ColorScheme.light(primary: AppColors.primary),
            ),
            child: child!,
          );
        },
        initialDate: _determineInitialDate(),
        firstDate: _determineInitialDate(),
        lastDate: DateTime.now().add(const Duration(days: 14)));
    if (selectedDate != null) {
      _selectedDate = selectedDate.copyWith(
        year: selectedDate.year,
        month: selectedDate.month,
        day: selectedDate.day,
      );
      _dateController.text =
          DateFormat('EEEE, d MMM yyyy').format(selectedDate);
    }
  }

  DateTime _determineInitialDate() {
    return (_selectedDate != null)
        ? _selectedDate!
        : DateTime.now().add(const Duration(days: 1));
  }
}
