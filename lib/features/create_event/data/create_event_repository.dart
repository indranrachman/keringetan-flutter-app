// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/event_service.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/model/request/create_event_request.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/model/response/event_response.dart';

abstract class CreateEventRepository {
  Future<EventResponse?> createEvent(CreateEventRequest request);
}

@Injectable(as: CreateEventRepository)
class CreateEventRepositoryImpl implements CreateEventRepository {
  final EventService service;
  CreateEventRepositoryImpl({
    required this.service,
  });

  @override
  Future<EventResponse?> createEvent(CreateEventRequest request) async {
    var response = await Future.delayed(const Duration(seconds: 2))
        .then((value) => service.createEvent(request));
    return response.data;
  }
}
