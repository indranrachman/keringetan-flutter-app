// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:keringetan_consumer_flutter_app/core/apis/event/model/response/event_response.dart';

class CreateEventResponse {
  final EventResponse data;
  CreateEventResponse({
    required this.data,
  });

  CreateEventResponse copyWith({
    EventResponse? data,
  }) {
    return CreateEventResponse(
      data: data ?? this.data,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'data': data.toMap(),
    };
  }

  factory CreateEventResponse.fromMap(Map<String, dynamic> map) {
    return CreateEventResponse(
      data: EventResponse.fromMap(map['data'] as Map<String, dynamic>),
    );
  }

  String toJson() => json.encode(toMap());

  factory CreateEventResponse.fromJson(String source) =>
      CreateEventResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'CreateEventResponse(data: $data)';

  @override
  bool operator ==(covariant CreateEventResponse other) {
    if (identical(this, other)) return true;

    return other.data == data;
  }

  @override
  int get hashCode => data.hashCode;
}
