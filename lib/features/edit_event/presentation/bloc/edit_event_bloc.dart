// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/model/request/edit_event_request.dart';
import 'package:keringetan_consumer_flutter_app/features/edit_event/data/edit_event_repository.dart';
import 'package:keringetan_consumer_flutter_app/features/edit_event/presentation/mapper/edit_event_mapper.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';

part 'edit_event_event.dart';
part 'edit_event_state.dart';

@injectable
class EditEventBloc extends Bloc<EditEventEvent, EditEventState> {
  final EditEventRepository repository;
  final EditEventMapper mapper;
  EditEventBloc(
    this.repository,
    this.mapper,
  ) : super(EditEventInitial()) {
    on<SendEditEvent>((event, emit) async {
      var request = EditEventRequest(
        id: event.id,
        categoryId: event.categoryId,
        caption: event.caption,
        meetingNotes: event.meetingNotes,
        date: event.date,
        location: EditLocationRequest(
          name: event.locationName,
          latitude: event.locationLatitude,
          longitude: event.locationLongitude,
        ),
      );
      emit(EditEventLoading(isVisible: true));
      await repository
          .editEvent(request)
          .then(
            (response) =>
                emit(EditEventSuccess(model: mapper.toEventModel(response))),
          )
          .onError((error, stackTrace) {
        emit(EditEventFailed(errorMessage: error.toString()));
      });
      emit(EditEventLoading(isVisible: false));
    });
  }
}
