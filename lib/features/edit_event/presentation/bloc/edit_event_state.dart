// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'edit_event_bloc.dart';

sealed class EditEventState {}

class EditEventInitial extends EditEventState {}

class EditEventSuccess extends EditEventState {
  final EventModel model;
  EditEventSuccess({
    required this.model,
  });
}

class EditEventFailed extends EditEventState {
  final String errorMessage;
  EditEventFailed({
    this.errorMessage = '',
  });
}

class EditEventLoading extends EditEventState {
  final bool isVisible;
  EditEventLoading({
    this.isVisible = false,
  });
}
