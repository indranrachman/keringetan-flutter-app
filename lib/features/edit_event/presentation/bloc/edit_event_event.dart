// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'edit_event_bloc.dart';

abstract class EditEventEvent extends Equatable {
  const EditEventEvent();

  @override
  List<Object> get props => [];
}

class SendEditEvent extends EditEventEvent {
  final int id;
  final int categoryId;
  final String caption;
  final String meetingNotes;
  final String date;
  final String locationName;
  final double locationLatitude;
  final double locationLongitude;
  const SendEditEvent({
    this.id = 0,
    this.categoryId = 0,
    this.caption = '',
    this.meetingNotes = '',
    this.date = '',
    this.locationName = '',
    this.locationLatitude = 0.0,
    this.locationLongitude = 0.0,
  });
}
