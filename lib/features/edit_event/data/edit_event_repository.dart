// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/event_service.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/model/request/edit_event_request.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/model/response/event_response.dart';

abstract class EditEventRepository {
  Future<EventResponse> editEvent(EditEventRequest request);
}

@Injectable(as: EditEventRepository)
class EditEventRepositoryImpl implements EditEventRepository {
  final EventService service;
  EditEventRepositoryImpl({
    required this.service,
  });
  @override
  Future<EventResponse> editEvent(EditEventRequest request) async {
    var response = await Future.delayed(const Duration(seconds: 2))
        .then((value) => service.editEvent(request));
    return response.data;
  }
}
