// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:keringetan_consumer_flutter_app/core/apis/event/model/response/event_response.dart';

class EditEventResponse {
  final EventResponse data;
  EditEventResponse({
    required this.data,
  });

  EditEventResponse copyWith({
    EventResponse? data,
  }) {
    return EditEventResponse(
      data: data ?? this.data,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'data': data.toMap(),
    };
  }

  factory EditEventResponse.fromMap(Map<String, dynamic> map) {
    return EditEventResponse(
      data: EventResponse.fromMap(map['data'] as Map<String, dynamic>),
    );
  }

  String toJson() => json.encode(toMap());

  factory EditEventResponse.fromJson(String source) =>
      EditEventResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'EditEventResponse(data: $data)';

  @override
  bool operator ==(covariant EditEventResponse other) {
    if (identical(this, other)) return true;

    return other.data == data;
  }

  @override
  int get hashCode => data.hashCode;
}
