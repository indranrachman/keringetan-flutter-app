// ignore_for_file: public_member_api_docs, sort_constructors_first
class NotificationModel {
  NotificationModel({
    required this.id,
    required this.title,
    required this.subTitle,
    required this.timeStamp,
    required this.isRead,
    required this.deeplink,
  });

  final String id;
  final String title;
  final String subTitle;
  final String timeStamp;
  final bool isRead;
  final String deeplink;

  @override
  String toString() {
    return 'NotificationModel(id: $id, title: $title, subTitle: $subTitle, timeStamp: $timeStamp, isRead: $isRead, deeplink: $deeplink)';
  }

  NotificationModel copyWith(
      {String? id,
      String? title,
      String? subTitle,
      String? timeStamp,
      bool? isRead,
      String? deeplink}) {
    return NotificationModel(
        id: id ?? this.id,
        title: title ?? this.title,
        subTitle: subTitle ?? this.subTitle,
        timeStamp: timeStamp ?? this.timeStamp,
        isRead: isRead ?? this.isRead,
        deeplink: deeplink ?? this.deeplink);
  }
}
