import 'package:flutter/material.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';
import 'package:keringetan_consumer_flutter_app/features/notifications/presentation/model/notification_model.dart';

class NotificationItem extends StatelessWidget {
  const NotificationItem({super.key, required this.model});

  final NotificationModel model;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.light,
      child: Padding(
        padding: const EdgeInsets.only(
          left: 16.0,
          right: 16,
          top: 12,
          bottom: 12,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    model.title,
                    style: AppStyles.anekLatinSemiBold.copyWith(),
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                const SizedBox(width: 16),
                Text(
                  model.timeStamp,
                  style: AppStyles.anekLatinRegular.copyWith(
                    fontSize: 10,
                    color: AppColors.textHint,
                  ),
                )
              ],
            ),
            const SizedBox(height: 4),
            Text(
              model.subTitle,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              textAlign: TextAlign.start,
              style: AppStyles.anekLatinRegular.copyWith(
                fontSize: 12,
                color: AppColors.textHint,
              ),
            )
          ],
        ),
      ),
    );
  }
}
