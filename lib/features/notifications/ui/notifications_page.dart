import 'package:flutter/material.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/data_faker.dart';
import 'package:keringetan_consumer_flutter_app/features/empty/ui/empty_state.dart';
import 'package:keringetan_consumer_flutter_app/features/notifications/ui/widgets/notification_item.dart';

class NotificationsPage extends StatefulWidget {
  const NotificationsPage({super.key});

  @override
  State<NotificationsPage> createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  final notifications = DataFaker.notifications;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.light,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new),
          onPressed: () {
            Navigator.pop(context, null);
          },
          color: AppColors.primary,
        ),
        automaticallyImplyLeading: false,
        elevation: 0.5,
        centerTitle: true,
        title: Text(
          'Notifications',
          style: AppStyles.anekLatinSemiBold.copyWith(
            fontSize: 16,
            color: AppColors.primary,
          ),
        ),
        backgroundColor: AppColors.light,
      ),
      body: Container(
        color: AppColors.light,
        child: SafeArea(
          maintainBottomViewPadding: true,
          child: _handleContentView(),
        ),
      ),
    );
  }

  Widget _handleContentView() {
    if (notifications.isEmpty) {
      return Center(
        child: EmptyState(
          model: EmptyStateModel(
            title: "No Notifications Found..",
            subTitle: "Don't worry come again later to check it 😎",
          ),
        ),
      );
    } else {
      return ListView.separated(
        itemCount: notifications.length,
        itemBuilder: (context, index) {
          return NotificationItem(model: notifications[index]);
        },
        separatorBuilder: (context, index) {
          return const Divider(height: 2);
        },
      );
    }
  }
}
