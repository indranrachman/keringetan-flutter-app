import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/google/firebase_sign_up_provider.dart';
import 'package:keringetan_consumer_flutter_app/core/extensions/context_ext.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  var _isLoadingWithGoogle = false;
  final _emailController = TextEditingController();
  final _nameController = TextEditingController();
  final _passwordController = TextEditingController();
  var _isPasswordFilled = false;
  var _isEmailFilled = false;
  var _isNameFilled = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.light,
      resizeToAvoidBottomInset: true,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios_new),
          onPressed: () {
            Navigator.pop(context, null);
          },
          color: AppColors.primary,
        ),
        centerTitle: true,
        elevation: 0,
        backgroundColor: AppColors.light,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Container(
          color: AppColors.light,
          child: SizedBox(
            width: double.infinity,
            child: SafeArea(
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        "assets/images/img_sign_up.svg",
                        height: 112,
                        width: 112,
                      ),
                      const SizedBox(height: 24),
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          'Hi again!',
                          style: AppStyles.anekLatinSemiBold
                              .copyWith(fontSize: 36),
                        ),
                      ),
                      const SizedBox(height: 8),
                      Align(
                        alignment: Alignment.center,
                        child: Text(
                          "Just count 1,2,3 and your account will be created!",
                          textAlign: TextAlign.center,
                          style: AppStyles.anekLatinRegular.copyWith(
                            fontSize: 12,
                            color: AppColors.textCaption,
                          ),
                        ),
                      ),
                      const SizedBox(height: 24),
                      TextField(
                        controller: _emailController,
                        maxLines: 1,
                        keyboardType: TextInputType.emailAddress,
                        style:
                            AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                        cursorColor: AppColors.primary,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.email),
                          prefixIconColor: AppColors.primary,
                          suffixIcon: Visibility(
                            visible: _isEmailFilled,
                            child: GestureDetector(
                              child: Icon(
                                Icons.close,
                                color: AppColors.primary,
                              ),
                              onTap: () {
                                setState(() {
                                  _isEmailFilled = false;
                                });
                                _emailController.clear();
                              },
                            ),
                          ),
                          labelText: "johndoe@xyz.com",
                          labelStyle: AppStyles.anekLatinRegular.copyWith(
                            fontSize: 12,
                            color: AppColors.textHint,
                          ),
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          floatingLabelStyle:
                              AppStyles.anekLatinRegular.copyWith(
                            color: AppColors.primary,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.primary, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          isDense: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                        ),
                        enableSuggestions: false,
                      ),
                      const SizedBox(height: 16),
                      TextField(
                        onChanged: (value) {
                          if (value.isNotEmpty && !_isNameFilled) {
                            setState(() {
                              _isNameFilled = true;
                            });
                          }
                          if (value.isEmpty && _isNameFilled) {
                            setState(() {
                              _isNameFilled = false;
                            });
                          }
                        },
                        controller: _nameController,
                        maxLines: 1,
                        keyboardType: TextInputType.emailAddress,
                        style:
                            AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                        cursorColor: AppColors.primary,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.face),
                          prefixIconColor: AppColors.primary,
                          suffixIcon: Visibility(
                            visible: _isNameFilled,
                            child: GestureDetector(
                              child: Icon(
                                Icons.close,
                                color: AppColors.primary,
                              ),
                              onTap: () {
                                setState(() {
                                  _isNameFilled = false;
                                });
                                _nameController.clear();
                              },
                            ),
                          ),
                          labelText: "Your name max 25 chars",
                          labelStyle: AppStyles.anekLatinRegular.copyWith(
                            fontSize: 12,
                            color: AppColors.textHint,
                          ),
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          floatingLabelStyle:
                              AppStyles.anekLatinRegular.copyWith(
                            color: AppColors.primary,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.primary, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          isDense: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                        ),
                        enableSuggestions: false,
                      ),
                      const SizedBox(height: 16),
                      TextField(
                        onChanged: (value) {
                          if (value.isNotEmpty && !_isPasswordFilled) {
                            setState(() {
                              _isPasswordFilled = true;
                            });
                          }
                          if (value.isEmpty && _isPasswordFilled) {
                            setState(() {
                              _isPasswordFilled = false;
                            });
                          }
                        },
                        controller: _passwordController,
                        keyboardType: TextInputType.visiblePassword,
                        maxLines: 1,
                        style:
                            AppStyles.anekLatinRegular.copyWith(fontSize: 12),
                        cursorColor: AppColors.primary,
                        decoration: InputDecoration(
                          prefixIcon: const Icon(Icons.lock),
                          prefixIconColor: AppColors.primary,
                          suffixIcon: Visibility(
                            visible: _isPasswordFilled,
                            child: GestureDetector(
                              child: Icon(
                                Icons.close,
                                color: AppColors.primary,
                              ),
                              onTap: () {
                                setState(() {
                                  _isPasswordFilled = false;
                                });
                                _passwordController.clear();
                              },
                            ),
                          ),
                          labelText: "Password min 8 characters",
                          labelStyle: AppStyles.anekLatinRegular.copyWith(
                            fontSize: 12,
                            color: AppColors.textHint,
                          ),
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          floatingLabelStyle:
                              AppStyles.anekLatinRegular.copyWith(
                            color: AppColors.primary,
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: AppColors.primary, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          isDense: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: AppColors.cardBorder, width: 1),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                        ),
                        enableSuggestions: false,
                      ),
                      const SizedBox(height: 8),
                      SizedBox(
                        width: double.infinity,
                        child: ElevatedButton.icon(
                          icon: _isLoadingWithGoogle
                              ? Container(
                                  width: 20,
                                  height: 20,
                                  padding: const EdgeInsets.all(2.0),
                                  child: CircularProgressIndicator(
                                    color: AppColors.light,
                                    strokeWidth: 3,
                                  ),
                                )
                              : const SizedBox.shrink(),
                          style: AppStyles.primaryButton,
                          onPressed: () {
                            _handleSignUp(context);
                          },
                          label: Text(
                            _isLoadingWithGoogle ? "" : "Sign up",
                            style: AppStyles.anekLatinBold.copyWith(
                              color: AppColors.light,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Positioned(
                    bottom: 0,
                    child: Center(
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text: "Already have an account?",
                            style: AppStyles.anekLatinRegular.copyWith(
                              color: AppColors.primary,
                              fontSize: 12,
                            ),
                            children: [
                              TextSpan(
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.pop(context);
                                    },
                                  text: " Login",
                                  style: AppStyles.anekLatinBold.copyWith(
                                    color: AppColors.primary,
                                    fontSize: 12,
                                  ))
                            ]),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _handleSignUp(BuildContext context) async {
    setState(() {
      _isLoadingWithGoogle = true;
    });
    FirebaseSignUpProvider.signUpWithEmailAndPassword(
      _emailController.text,
      _passwordController.text,
    ).then((result) => _handleResult(result, context));
  }

  _handleResult(
    SignUpResult result,
    BuildContext context,
  ) {
    setState(() {
      _isLoadingWithGoogle = false;
    });
    if (result.error != null) {
      context.showSnackBar(result.error);
    }
    if (result.user != null) {
      context.showSnackBar(result.user?.email.toString());
    }
  }
}
