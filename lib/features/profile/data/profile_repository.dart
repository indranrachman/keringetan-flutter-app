// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';

import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/google/google_sign_in_provider.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/user/model/user_logout_request.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/user/user_service.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/pref_utils.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';

abstract class ProfileRepository {
  Future<UserModel?> getProfile();
  Future<void> signOut();
}

@Injectable(as: ProfileRepository)
class ProfileRepositoryImpl implements ProfileRepository {
  final GoogleSignInProvider signInProvider;
  final UserService userService;
  final PrefUtils prefUtils;
  ProfileRepositoryImpl({
    required this.signInProvider,
    required this.userService,
    required this.prefUtils,
  });

  @override
  Future<UserModel?> getProfile() async {
    var json = await Future.value(prefUtils.getUser())
        .then((value) => prefUtils.getUser());
    if (json.isEmpty) {
      return null;
    } else {
      return UserModel.fromJson(json);
    }
  }

  @override
  Future<void> signOut() async {
    await signInProvider
        .signOut()
        .then((value) => userService.signOut(UserLogoutRequest(id: 90)))
        .then((value) => prefUtils.clearPreferencesData());
  }
}
