import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keringetan_consumer_flutter_app/core/deps/dependency_injection.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';
import 'package:keringetan_consumer_flutter_app/features/edit_profile/ui/edit_profile.dart';
import 'package:keringetan_consumer_flutter_app/features/login/ui/login_page.dart';
import 'package:keringetan_consumer_flutter_app/features/profile/presentation/bloc/profile_bloc.dart';
import 'package:keringetan_consumer_flutter_app/features/profile/presentation/bloc/profile_state.dart';
import 'package:keringetan_consumer_flutter_app/widgets/plain_pop_up.dart';

class ProfilePage extends StatelessWidget {
  static Widget builder(BuildContext context) {
    return BlocProvider<ProfileBloc>(
      create: (context) => getIt<ProfileBloc>()..add(FetchProfileEvent()),
      child: const ProfilePage(),
    );
  }

  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    //BlocProvider.of<ProfileBloc>(context).add(SignOutEvent());
    return BlocConsumer<ProfileBloc, ProfileState>(
      listener: (context, state) {
        _handleSignOutState(context, state);
      },
      builder: (context, state) {
        return Scaffold(
            backgroundColor: AppColors.cardBackgroundActive,
            appBar: AppBar(
              automaticallyImplyLeading: false,
              centerTitle: false,
              elevation: 0.5,
              actions: [
                Visibility(
                  visible: state.isLoggedIn,
                  child: IconButton(
                    splashRadius: 24,
                    onPressed: () {
                      Navigator.of(context, rootNavigator: true)
                          .push(MaterialPageRoute(builder: (context) {
                        return EditProfilePage.builder(context, state.model);
                      }));
                    },
                    color: AppColors.primary,
                    icon: const Icon(Icons.edit_outlined),
                  ),
                )
              ],
              title: Text(
                'My Profile',
                style: AppStyles.anekLatinBold.copyWith(
                  fontSize: 16,
                  color: AppColors.primary,
                ),
              ),
              backgroundColor: AppColors.light,
            ),
            body: SizedBox(
              width: double.infinity,
              child: _handleContent(context, state),
            ));
      },
    );
  }

  void _handleSignOutState(
    BuildContext context,
    ProfileState state,
  ) {
    if (state.isSignOutSuccess == true) {
      BlocProvider.of<ProfileBloc>(context).add(FetchProfileEvent());
    } else if (state.isSignOutSuccess == false) {
      showModalBottomSheet(
        useSafeArea: true,
        context: context,
        showDragHandle: true,
        useRootNavigator: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(20.0),
          ),
        ),
        isScrollControlled: true,
        builder: (context) {
          return PlainPopUp(
            model: PlainPopUpModel(
                title: "Terjadi Kesalahan",
                subTitle: "Silakan coba beberapa saat lagi",
                onClicked: () {
                  Navigator.pop(context);
                }),
          );
        },
      );
    }
  }

  Widget _handleContent(
    BuildContext context,
    ProfileState state,
  ) {
    if (state.isProfileLoading) {
      return const Padding(
        padding: EdgeInsets.all(16.0),
        child: Center(
          child: CircularProgressIndicator.adaptive(),
        ),
      );
    } else {
      return Column(
        children: [
          Container(
            width: double.infinity,
            color: AppColors.light,
            child: Padding(
              padding: const EdgeInsets.only(
                top: 16,
                left: 16,
                right: 16,
                bottom: 16,
              ),
              child: Column(
                children: [
                  CachedNetworkImage(
                    imageUrl: state.model.avatar,
                    imageBuilder: (context, imageProvider) => Container(
                      width: 96,
                      height: 96,
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(16)),
                        border: Border.all(
                          width: 2,
                          color: AppColors.light,
                        ),
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    placeholder: (context, url) =>
                        const CircularProgressIndicator.adaptive(),
                    errorWidget: (context, url, error) =>
                        const Icon(Icons.face),
                  ),
                  const SizedBox(height: 8),
                  Text(
                    state.model.name,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: AppStyles.anekLatinSemiBold.copyWith(
                      color: AppColors.textPrimary,
                    ),
                  ),
                  // Visibility(
                  //     visible: state.model.gender.isNotEmpty,
                  //     child: const SizedBox(height: 8)),
                  // Visibility(
                  //   visible: state.model.gender.isNotEmpty,
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.center,
                  //     children: [
                  //       Container(
                  //         decoration: BoxDecoration(
                  //           color: AppColors.pillMutedBackground,
                  //           shape: BoxShape.rectangle,
                  //           borderRadius:
                  //               const BorderRadius.all(Radius.circular(16)),
                  //         ),
                  //         child: Padding(
                  //           padding: const EdgeInsets.only(
                  //             top: 4.0,
                  //             bottom: 4,
                  //             right: 8,
                  //             left: 8,
                  //           ),
                  //           child: Text(
                  //             state.model.gender,
                  //             style: AppStyles.anekLatinSemiBold.copyWith(
                  //               fontSize: 12,
                  //               color: AppColors.pillMutedText,
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // ),
                  Visibility(
                      visible: state.model.bio.isNotEmpty,
                      child: const SizedBox(height: 8)),
                  Visibility(
                    visible: state.model.bio.isNotEmpty,
                    child: Text(
                      state.model.bio,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: AppStyles.anekLatinRegular.copyWith(
                        fontSize: 12,
                        color: AppColors.textHint,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            width: double.infinity,
            color: AppColors.cardBackgroundActive,
            child: Padding(
              padding: const EdgeInsets.only(
                top: 12,
                bottom: 12,
                left: 12,
                right: 12,
              ),
              child: Container(
                decoration: BoxDecoration(
                  color: AppColors.light,
                  borderRadius: const BorderRadius.all(Radius.circular(8)),
                  shape: BoxShape.rectangle,
                ),
                child: Column(
                  children: [
                    ListTile(
                      onTap: () {
                        _handleAuthHandling(context, state);
                      },
                      title: Text(
                        state.isLoggedIn ? "Sign Out" : "Sign In",
                        style:
                            AppStyles.anekLatinSemiBold.copyWith(fontSize: 12),
                      ),
                      leading: Icon(
                        Icons.logout,
                        color: AppColors.primary,
                      ),
                      trailing: state.isSignOutLoading == true
                          ? const SizedBox(
                              height: 16,
                              width: 16,
                              child: CircularProgressIndicator.adaptive(),
                            )
                          : Icon(
                              Icons.chevron_right,
                              color: AppColors.primary,
                            ),
                    ),
                    const Divider(height: 2),
                    ListTile(
                      onTap: () {},
                      title: Text(
                        "Language",
                        style:
                            AppStyles.anekLatinSemiBold.copyWith(fontSize: 12),
                      ),
                      leading: Icon(
                        Icons.language_outlined,
                        color: AppColors.primary,
                      ),
                      trailing: Icon(
                        Icons.chevron_right,
                        color: AppColors.primary,
                      ),
                    ),
                    const Divider(height: 2),
                    ListTile(
                      onTap: () {},
                      title: Text(
                        "Rate Us",
                        style:
                            AppStyles.anekLatinSemiBold.copyWith(fontSize: 12),
                      ),
                      leading: Icon(
                        Icons.star_border_outlined,
                        color: AppColors.primary,
                      ),
                      trailing: Text(
                        "v0.1.0",
                        style: AppStyles.anekLatinSemiBold.copyWith(
                          fontSize: 12,
                          color: AppColors.textHint,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      );
    }
  }

  _handleAuthHandling(
    BuildContext context,
    ProfileState state,
  ) {
    if (state.isLoggedIn) {
      BlocProvider.of<ProfileBloc>(context).add(SignOutEvent());
    } else {
      var loginPage = LoginPage.builder(context);
      Navigator.of(
        context,
        rootNavigator: true,
      ).push(MaterialPageRoute(builder: (context) {
        return loginPage;
      })).then((value) {
        if (value != null && value) {
          BlocProvider.of<ProfileBloc>(context).add(FetchProfileEvent());
        }
      });
    }
  }
}
