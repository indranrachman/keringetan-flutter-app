abstract class ProfileStateMapper<ProfileState> {
  ProfileState map(ProfileState profile);
}
