// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';
import 'package:keringetan_consumer_flutter_app/features/profile/data/profile_repository.dart';
import 'package:keringetan_consumer_flutter_app/features/profile/presentation/bloc/profile_state.dart';

part 'profile_event.dart';

@injectable
class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final ProfileRepository repository;

  ProfileBloc(this.repository) : super(const ProfileState(model: UserModel())) {
    on<FetchProfileEvent>(_fetchProfile);
    on<SignOutEvent>(_signOut);
  }
  FutureOr<void> _signOut(event, emit) async {
    emit(state.copyWith(
      isSignOutLoading: true,
      isSignOutSuccess: null,
    ));
    await repository.signOut();
    emit(state.copyWith(
      isSignOutLoading: false,
      isLoggedIn: false,
      isSignOutSuccess: true,
    ));
  }

  FutureOr<void> _fetchProfile(event, emit) async {
    emit(state.copyWith(
      isProfileLoading: true,
      isSignOutSuccess: null,
    ));
    var model = await repository.getProfile();
    emit(state.copyWith(
      isLoggedIn: model != null,
      isProfileLoading: false,
      model: model,
    ));
  }
}
