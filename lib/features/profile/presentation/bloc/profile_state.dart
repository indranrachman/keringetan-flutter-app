// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';

class ProfileState extends Equatable {
  final UserModel model;
  final bool isProfileLoading;
  final bool isSignOutLoading;
  final bool? isSignOutSuccess;
  final bool isLoggedIn;
  const ProfileState({
    required this.model,
    this.isProfileLoading = true,
    this.isSignOutLoading = false,
    this.isSignOutSuccess,
    this.isLoggedIn = false,
  });

  ProfileState copyWith({
    UserModel? model,
    bool? isProfileLoading,
    bool? isSignOutLoading,
    bool? isSignOutSuccess,
    bool? isLoggedIn,
  }) {
    return ProfileState(
      model: model ?? this.model,
      isProfileLoading: isProfileLoading ?? this.isProfileLoading,
      isSignOutLoading: isSignOutLoading ?? this.isSignOutLoading,
      isSignOutSuccess: isSignOutSuccess,
      isLoggedIn: isLoggedIn ?? this.isLoggedIn,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'model': model.toMap(),
      'isProfileLoading': isProfileLoading,
      'isSignOutLoading': isSignOutLoading,
      'isSignOutSuccess': isSignOutSuccess,
      'isLoggedIn': isLoggedIn,
    };
  }

  factory ProfileState.fromMap(Map<String, dynamic> map) {
    return ProfileState(
      model: UserModel.fromMap(map['model'] as Map<String, dynamic>),
      isProfileLoading: map['isProfileLoading'] as bool,
      isSignOutLoading: map['isSignOutLoading'] as bool,
      isSignOutSuccess: map['isSignOutSuccess'] != null
          ? map['isSignOutSuccess'] as bool
          : null,
      isLoggedIn: map['isLoggedIn'] as bool,
    );
  }

  String toJson() => json.encode(toMap());

  factory ProfileState.fromJson(String source) =>
      ProfileState.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      model,
      isProfileLoading,
      isSignOutLoading,
      isSignOutSuccess,
      isLoggedIn,
    ];
  }
}
