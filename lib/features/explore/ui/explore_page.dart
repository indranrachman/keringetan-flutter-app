import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keringetan_consumer_flutter_app/core/deps/dependency_injection.dart';
import 'package:keringetan_consumer_flutter_app/core/extensions/context_ext.dart';
import 'package:keringetan_consumer_flutter_app/core/extensions/string_ext.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';
import 'package:keringetan_consumer_flutter_app/features/empty/ui/empty_state.dart';
import 'package:keringetan_consumer_flutter_app/features/event_detail/ui/event_detail_page.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/bloc/explore_bloc.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/bloc/explore_state.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_filter_selection_model.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/ui/explore_filter_popup.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/ui/widgets/explore_item.dart';
import 'package:keringetan_consumer_flutter_app/features/notifications/ui/notifications_page.dart';

class ExplorePage extends StatefulWidget {
  const ExplorePage({super.key});

  static Widget builder(BuildContext context) {
    return BlocProvider<ExploreBloc>(
      create: (context) =>
          getIt<ExploreBloc>()..add(const ExploreFetchListEvent()),
      child: const ExplorePage(),
    );
  }

  @override
  State<ExplorePage> createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  var model = ExploreFilterSelectionModel();
  var badgeCount = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.cardBackgroundActive,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0.5,
        centerTitle: false,
        actions: [
          BlocBuilder<ExploreBloc, ExploreState>(
            builder: (context, state) {
              return IconButton(
                splashRadius: 24,
                onPressed: () {
                  showModalBottomSheet(
                    barrierColor: AppColors.bottomSheetBarrierColor,
                    backgroundColor: AppColors.light,
                    showDragHandle: true,
                    useSafeArea: true,
                    context: context,
                    useRootNavigator: true,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.vertical(
                        top: Radius.circular(20.0),
                      ),
                    ),
                    builder: (context) {
                      return ExploreFilterPopUp(
                        model: model,
                      );
                    },
                  ).then((value) => _handleFilterSelection(context, value));
                },
                color: AppColors.primary,
                icon: Badge.count(
                  isLabelVisible: badgeCount > 0,
                  count: badgeCount,
                  child: const Icon(Icons.filter_list),
                ),
              );
            },
          ),
          IconButton(
            splashRadius: 24,
            onPressed: () {
              _handleNotificationRedirections(context);
            },
            color: AppColors.primary,
            icon: const Icon(Icons.notifications_outlined),
          )
        ],
        title: Text(
          'Explore',
          style: AppStyles.anekLatinBold.copyWith(
            fontSize: 16,
            color: AppColors.primary,
          ),
        ),
        backgroundColor: AppColors.light,
      ),
      body: SafeArea(
        child: BlocConsumer<ExploreBloc, ExploreState>(
          listener: (context, state) {
            if (state.error != null) {
              context.showSnackBar(state.error);
            }
          },
          builder: (context, state) {
            return _handleContentView(state);
          },
        ),
      ),
    );
  }

  Widget _handleContentView(ExploreState state) {
    return LayoutBuilder(
      builder: (context, constraint) => RefreshIndicator.adaptive(
        onRefresh: () async {
          var stream = context.read<ExploreBloc>().stream.first;
          context.read<ExploreBloc>().add(ExploreRefetchListEvent());
          await stream;
        },
        child: _renderExploreList(state, constraint),
      ),
    );
  }

  Widget _renderExploreList(
    ExploreState state,
    BoxConstraints constraint,
  ) {
    if (state.showLoading == true) {
      return const Center(
        child: CircularProgressIndicator.adaptive(),
      );
    }
    if (state.items == null) {
      return const SizedBox.shrink();
    }
    if (state.items?.isEmpty == true) {
      return ListView(
        physics: const AlwaysScrollableScrollPhysics(),
        children: [
          ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: constraint.maxHeight,
            ),
            child: EmptyState(
              model: EmptyStateModel(
                title: "No Events Found :(",
                subTitle: "Go create one and find your sport partner!",
                ctaText: "Refresh",
              ),
            ),
          )
        ],
      );
    }
    return Padding(
      padding: const EdgeInsets.all(12),
      child: ListView.separated(
        clipBehavior: Clip.none,
        separatorBuilder: (context, index) {
          return const SizedBox(height: 12);
        },
        itemCount: state.items!.length,
        itemBuilder: (context, index) {
          return ExploreItem(
            model: state.items![index],
            onExploreItemClicked: (model) {
              showModalBottomSheet(
                barrierColor: AppColors.bottomSheetBarrierColor,
                backgroundColor: AppColors.light,
                isScrollControlled: true,
                showDragHandle: true,
                useSafeArea: true,
                context: context,
                useRootNavigator: true,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(20.0),
                  ),
                ),
                builder: (context) {
                  return EventDetailPage(model: model);
                },
              ).then((value) => context.showSnackBar(value));
            },
          );
        },
      ),
    );
  }

  void _handleFilterSelection(BuildContext context, dynamic value) {
    model = value as ExploreFilterSelectionModel;
    setState(() {
      badgeCount =
          model.toMap().values.where((selection) => selection != null).length;
    });
    context.read<ExploreBloc>().add(
          ExploreFetchListEvent(
            state: model.selectedEventState,
            categoryId: int.tryParse(model.selectedCategory.orEmpty()),
          ),
        );
  }

  void _handleNotificationRedirections(BuildContext context) {
    Navigator.of(context, rootNavigator: true).push(
      MaterialPageRoute(
        builder: (context) {
          return const NotificationsPage();
        },
      ),
    );
  }
}
