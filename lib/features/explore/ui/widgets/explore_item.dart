import 'package:flutter/material.dart';

import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';

class ExploreItem extends StatelessWidget {
  final EventModel model;
  final Function(EventModel model) onExploreItemClicked;
  const ExploreItem({
    Key? key,
    required this.model,
    required this.onExploreItemClicked,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.cardBackground,
        borderRadius: const BorderRadius.all(Radius.circular(8)),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: const BorderRadius.all(Radius.circular(8)),
          onTap: () => onExploreItemClicked(model),
          child: Padding(
            padding:
                const EdgeInsets.only(left: 16, right: 16, top: 12, bottom: 12),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 36,
                      height: 36,
                      decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(4)),
                        image: DecorationImage(
                          image: NetworkImage(model.poster.avatar),
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    const SizedBox(width: 8),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          model.poster.name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: AppStyles.anekLatinSemiBold.copyWith(
                            fontSize: 14,
                          ),
                        ),
                        Row(
                          children: [
                            Text(
                              model.category.name,
                              style: AppStyles.anekLatinSemiBold.copyWith(
                                fontSize: 12,
                                color: AppColors.textCaption,
                              ),
                            ),
                            // const SizedBox(width: 4),
                            // Text(
                            //   "•",
                            //   style: AppStyles.anekLatinRegular.copyWith(
                            //     fontSize: 10,
                            //     color: AppColors.textHint,
                            //   ),
                            // ),
                            // const SizedBox(width: 4),
                            // Text(
                            //   model.event.timeStamp,
                            //   style: AppStyles.anekLatinRegular.copyWith(
                            //     fontSize: 10,
                            //     color: AppColors.textHint,
                            //   ),
                            // )
                          ],
                        ),
                      ],
                    ),
                    const Spacer(),
                    Text(
                      model.timeStamp,
                      style: AppStyles.anekLatinRegular.copyWith(
                        fontSize: 10,
                        color: AppColors.textHint,
                      ),
                    )
                    //EventStatusBadge(model: model),
                  ],
                ),
                const SizedBox(height: 12),
                Container(
                  width: double.infinity,
                  height: 160,
                  decoration: const BoxDecoration(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    image: DecorationImage(
                      image: NetworkImage(
                          'https://lh3.googleusercontent.com/p/AF1QipMPcJJhPaUv4VkBJ9WJA97ZZK6zjXl5BNBacZGA=s1360-w1360-h1020'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const SizedBox(height: 12),
                Column(
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.date_range,
                          size: 16,
                          color: AppColors.primary,
                        ),
                        const SizedBox(width: 4),
                        Text(
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          model.date,
                          style: AppStyles.anekLatinRegular.copyWith(
                            fontSize: 12,
                            color: AppColors.textCaption,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 2),
                    Row(
                      children: [
                        Icon(
                          Icons.pin_drop,
                          size: 16,
                          color: AppColors.primary,
                        ),
                        const SizedBox(width: 4),
                        Text(
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          model.location.name,
                          style: AppStyles.anekLatinRegular.copyWith(
                            fontSize: 12,
                            color: AppColors.textCaption,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Visibility(
                  visible: model.caption.isNotEmpty,
                  child: Column(
                    children: [
                      const SizedBox(height: 8),
                      Text(
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        model.caption,
                        style: AppStyles.anekLatinRegular.copyWith(
                          fontSize: 12,
                          color: AppColors.textCaption,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
