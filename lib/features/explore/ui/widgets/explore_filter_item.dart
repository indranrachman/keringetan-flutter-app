// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_filter_model.dart';

class ExploreFilterItem extends StatelessWidget {
  const ExploreFilterItem({
    super.key,
    required this.model,
    required this.onItemClicked,
    required this.onItemUnClicked,
  });

  final ExploreFilterModel model;
  final Function(String id) onItemClicked;
  final Function() onItemUnClicked;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: (model.isSelected) ? AppColors.primary : AppColors.light,
        border: Border.all(
          width: 1,
          color: AppColors.cardBorder,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(24.0),
        ),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          borderRadius: BorderRadius.circular(24),
          onTap: _handleOnTap,
          child: Padding(
            padding: const EdgeInsets.only(
              top: 8,
              bottom: 8,
              left: 12,
              right: 12,
            ),
            child: Center(
              child: Text(
                model.name,
                style: AppStyles.anekLatinSemiBold.copyWith(
                  color: (model.isSelected)
                      ? AppColors.light
                      : AppColors.textPrimary,
                  fontSize: 12,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  _handleOnTap() {
    if (model.isSelected) {
      onItemUnClicked();
    } else {
      onItemClicked(model.id);
    }
  }
}
