// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:keringetan_consumer_flutter_app/core/themes/app_colors.dart';
import 'package:keringetan_consumer_flutter_app/core/themes/app_styles.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/data_faker.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_filter_selection_model.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/ui/widgets/explore_filter_item.dart';

class ExploreFilterPopUp extends StatefulWidget {
  final ExploreFilterSelectionModel model;
  const ExploreFilterPopUp({
    Key? key,
    required this.model,
  }) : super(key: key);

  @override
  State<ExploreFilterPopUp> createState() => _ExploreFilterPopUpState();
}

class _ExploreFilterPopUpState extends State<ExploreFilterPopUp> {
  var categoryFilters = DataFaker.categoryFilters;
  var statusFilters = DataFaker.statusFilters;
  String? categoryId;
  String? eventState;

  @override
  void initState() {
    super.initState();
    setState(() {
      categoryFilters = categoryFilters
          .map((e) =>
              e.copyWith(isSelected: e.id == widget.model.selectedCategory))
          .toList();
      categoryId = widget.model.selectedCategory;
      statusFilters = statusFilters
          .map((e) =>
              e.copyWith(isSelected: e.id == widget.model.selectedEventState))
          .toList();
      eventState = widget.model.selectedEventState;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.light,
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            bottom: 12,
            left: 16,
            right: 16,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Category',
                style: AppStyles.anekLatinBold,
              ),
              const SizedBox(height: 16),
              SizedBox(
                height: 36,
                child: ListView.separated(
                  clipBehavior: Clip.none,
                  shrinkWrap: true,
                  separatorBuilder: (context, index) =>
                      const SizedBox(width: 8),
                  itemCount: categoryFilters.length,
                  itemBuilder: (context, index) => ExploreFilterItem(
                    model: categoryFilters[index],
                    onItemClicked: (categoryId) {
                      setState(() {
                        categoryFilters = categoryFilters
                            .map((e) =>
                                e.copyWith(isSelected: e.id == categoryId))
                            .toList();
                        this.categoryId = categoryId;
                      });
                    },
                    onItemUnClicked: () {
                      setState(() {
                        _resetCategorySelection();
                      });
                    },
                  ),
                  scrollDirection: Axis.horizontal,
                ),
              ),
              const SizedBox(height: 24),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Event',
                  style: AppStyles.anekLatinBold,
                ),
              ),
              const SizedBox(height: 16),
              SizedBox(
                height: 36,
                child: ListView.separated(
                  clipBehavior: Clip.none,
                  shrinkWrap: true,
                  separatorBuilder: (context, index) =>
                      const SizedBox(width: 8),
                  itemCount: statusFilters.length,
                  itemBuilder: (context, index) => ExploreFilterItem(
                    model: statusFilters[index],
                    onItemClicked: (id) {
                      setState(() {
                        statusFilters = statusFilters
                            .map(
                              (e) => e.copyWith(isSelected: e.id == id),
                            )
                            .toList();
                        eventState = id;
                      });
                    },
                    onItemUnClicked: () {
                      setState(() {
                        _resetEventStateSelection();
                      });
                    },
                  ),
                  scrollDirection: Axis.horizontal,
                ),
              ),
              const SizedBox(height: 32),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: OutlinedButton(
                      style: OutlinedButton.styleFrom(
                        backgroundColor: _isFilterApplied()
                            ? AppColors.pillBlueBackground
                            : AppColors.pillMutedBackground,
                        textStyle: AppStyles.anekLatinBold.copyWith(
                          color: AppColors.primary,
                        ),
                        side: BorderSide.none,
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8.0),
                          ),
                        ),
                      ),
                      onPressed:
                          _isFilterApplied() ? () => _resetAllFilter() : null,
                      child: Text(
                        "Reset",
                        style: AppStyles.anekLatinBold.copyWith(
                          color: _isFilterApplied()
                              ? AppColors.primary
                              : AppColors.pillMutedText,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(width: 16),
                  Expanded(
                    child: ElevatedButton(
                      style: AppStyles.primaryButton,
                      onPressed: () {
                        Navigator.pop(
                            context,
                            ExploreFilterSelectionModel(
                              selectedCategory: categoryId,
                              selectedEventState: eventState,
                            ));
                      },
                      child: const Text("Apply"),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  bool _isFilterApplied() => categoryId != null || eventState != null;

  _resetAllFilter() {
    setState(() {
      _resetCategorySelection();
      _resetEventStateSelection();
    });
  }

  _resetCategorySelection() {
    categoryFilters =
        categoryFilters.map((e) => e.copyWith(isSelected: false)).toList();
    categoryId = null;
  }

  _resetEventStateSelection() {
    statusFilters =
        statusFilters.map((e) => e.copyWith(isSelected: false)).toList();

    eventState = null;
  }
}
