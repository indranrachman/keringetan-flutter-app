// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/event_service.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/model/request/get_events_request.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/model/response/event_response.dart';

abstract class ExploreRepository {
  Future<List<EventResponse>> getExploreList(GetEventsRequest request);
}

@Injectable(as: ExploreRepository)
class ExploreRespositoryImpl implements ExploreRepository {
  final EventService eventService;
  ExploreRespositoryImpl({
    required this.eventService,
  });
  @override
  Future<List<EventResponse>> getExploreList(GetEventsRequest request) async {
    var response = await eventService.getEvents(request);
    return response.data;
  }
}
