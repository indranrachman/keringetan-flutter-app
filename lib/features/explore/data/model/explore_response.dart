import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/model/response/event_response.dart';

// ignore_for_file: public_member_api_docs, sort_constructors_first

class ExploreResponse {
  final List<EventResponse> data;
  ExploreResponse({
    this.data = const [],
  });

  ExploreResponse copyWith({
    List<EventResponse>? data,
  }) {
    return ExploreResponse(
      data: data ?? this.data,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'data': data.map((x) => x.toMap()).toList(),
    };
  }

  factory ExploreResponse.fromMap(Map<String, dynamic> map) {
    return ExploreResponse(
      data: List<EventResponse>.from(
        (map['data'] as List).map<EventResponse>(
          (x) => EventResponse.fromMap(x as Map<String, dynamic>),
        ),
      ),
    );
  }

  String toJson() => json.encode(toMap());

  factory ExploreResponse.fromJson(String source) =>
      ExploreResponse.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'ExploreResponse(data: $data)';

  @override
  bool operator ==(covariant ExploreResponse other) {
    if (identical(this, other)) return true;

    return listEquals(other.data, data);
  }

  @override
  int get hashCode => data.hashCode;
}
