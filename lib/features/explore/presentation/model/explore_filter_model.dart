class ExploreFilterModel {
  ExploreFilterModel({
    required this.id,
    required this.name,
    required this.isSelected,
  });
  final String id;
  final String name;
  final bool isSelected;

  ExploreFilterModel copyWith({
    String? id,
    String? name,
    bool? isSelected,
  }) {
    return ExploreFilterModel(
      id: id ?? this.id,
      name: name ?? this.name,
      isSelected: isSelected ?? this.isSelected,
    );
  }
}
