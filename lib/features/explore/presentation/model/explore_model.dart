// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class ParticipantModel {
  final String id;
  final String name;
  final String avatar;
  ParticipantModel({
    this.id = '',
    this.name = '',
    this.avatar = '',
  });

  ParticipantModel copyWith({
    String? id,
    String? name,
    String? avatar,
  }) {
    return ParticipantModel(
      id: id ?? this.id,
      name: name ?? this.name,
      avatar: avatar ?? this.avatar,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'avatar': avatar,
    };
  }

  factory ParticipantModel.fromMap(Map<String, dynamic> map) {
    return ParticipantModel(
      id: map['id'] as String,
      name: map['name'] as String,
      avatar: map['avatar'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory ParticipantModel.fromJson(String source) =>
      ParticipantModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'ParticipantModel(id: $id, name: $name, avatar: $avatar)';

  @override
  bool operator ==(covariant ParticipantModel other) {
    if (identical(this, other)) return true;

    return other.id == id && other.name == name && other.avatar == avatar;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ avatar.hashCode;
}

class UserModel extends Equatable {
  final int id;
  final String name;
  final String level;
  final String avatar;
  final String email;
  final String gender;
  final String bio;
  final String token;
  const UserModel({
    this.id = 0,
    this.name = '',
    this.level = '',
    this.avatar = '',
    this.email = '',
    this.gender = '',
    this.bio = '',
    this.token = '',
  });

  UserModel copyWith({
    int? id,
    String? name,
    String? level,
    String? avatar,
    String? email,
    String? gender,
    String? bio,
    String? token,
  }) {
    return UserModel(
      id: id ?? this.id,
      name: name ?? this.name,
      level: level ?? this.level,
      avatar: avatar ?? this.avatar,
      email: email ?? this.email,
      gender: gender ?? this.gender,
      bio: bio ?? this.bio,
      token: token ?? this.token,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'level': level,
      'avatar': avatar,
      'email': email,
      'gender': gender,
      'bio': bio,
      'token': token,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      id: map['id'] as int,
      name: map['name'] as String,
      level: map['level'] as String,
      avatar: map['avatar'] as String,
      email: map['email'] as String,
      gender: map['gender'] as String,
      bio: map['bio'] as String,
      token: map['token'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      id,
      name,
      level,
      avatar,
      email,
      gender,
      bio,
      token,
    ];
  }
}

class EventModel {
  int id;
  CategoryModel category;
  String date;
  String time;
  String timeStamp;
  LocationModel location;
  String meetingNotes;
  String caption;
  String state;
  List<int> participants;
  UserModel poster;
  EventModel({
    this.id = 0,
    required this.category,
    this.date = '',
    this.time = '',
    this.timeStamp = '',
    required this.location,
    this.meetingNotes = '',
    this.caption = '',
    this.state = '',
    this.participants = const [],
    required this.poster,
  });

  EventModel copyWith({
    int? id,
    CategoryModel? category,
    String? date,
    String? time,
    String? timeStamp,
    LocationModel? location,
    String? meetingNotes,
    String? caption,
    String? state,
    List<int>? participants,
    UserModel? poster,
  }) {
    return EventModel(
      id: id ?? this.id,
      category: category ?? this.category,
      date: date ?? this.date,
      time: time ?? this.time,
      timeStamp: timeStamp ?? this.timeStamp,
      location: location ?? this.location,
      meetingNotes: meetingNotes ?? this.meetingNotes,
      caption: caption ?? this.caption,
      state: state ?? this.state,
      participants: participants ?? this.participants,
      poster: poster ?? this.poster,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'category': category.toMap(),
      'date': date,
      'time': time,
      'timeStamp': timeStamp,
      'location': location.toMap(),
      'meetingNotes': meetingNotes,
      'caption': caption,
      'state': state,
      'participants': participants,
      'poster': poster.toMap(),
    };
  }

  factory EventModel.fromMap(Map<String, dynamic> map) {
    return EventModel(
      id: map['id'] as int,
      category: CategoryModel.fromMap(map['category'] as Map<String, dynamic>),
      date: map['date'] as String,
      time: map['time'] as String,
      timeStamp: map['timeStamp'] as String,
      location: LocationModel.fromMap(map['location'] as Map<String, dynamic>),
      meetingNotes: map['meetingNotes'] as String,
      caption: map['caption'] as String,
      state: map['state'] as String,
      participants: List<int>.from(map['participants'] as List<int>),
      poster: UserModel.fromMap(map['poster'] as Map<String, dynamic>),
    );
  }

  String toJson() => json.encode(toMap());

  factory EventModel.fromJson(String source) =>
      EventModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'EventModel(id: $id, category: $category, date: $date, time: $time, timeStamp: $timeStamp, location: $location, meetingNotes: $meetingNotes, caption: $caption, state: $state, participants: $participants, poster: $poster)';
  }

  @override
  bool operator ==(covariant EventModel other) {
    if (identical(this, other)) return true;

    return other.id == id &&
        other.category == category &&
        other.date == date &&
        other.time == time &&
        other.timeStamp == timeStamp &&
        other.location == location &&
        other.meetingNotes == meetingNotes &&
        other.caption == caption &&
        other.state == state &&
        listEquals(other.participants, participants) &&
        other.poster == poster;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        category.hashCode ^
        date.hashCode ^
        time.hashCode ^
        timeStamp.hashCode ^
        location.hashCode ^
        meetingNotes.hashCode ^
        caption.hashCode ^
        state.hashCode ^
        participants.hashCode ^
        poster.hashCode;
  }
}

class CategoryModel {
  final int id;
  final String name;
  bool isSelected;
  CategoryModel({
    this.id = 0,
    this.name = '',
    this.isSelected = false,
  });

  CategoryModel copyWith({
    int? id,
    String? name,
    bool? isSelected,
  }) {
    return CategoryModel(
      id: id ?? this.id,
      name: name ?? this.name,
      isSelected: isSelected ?? this.isSelected,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'id': id,
      'name': name,
      'isSelected': isSelected,
    };
  }

  factory CategoryModel.fromMap(Map<String, dynamic> map) {
    return CategoryModel(
      id: map['id'] as int,
      name: map['name'] as String,
      isSelected: map['isSelected'] as bool,
    );
  }

  String toJson() => json.encode(toMap());

  factory CategoryModel.fromJson(String source) =>
      CategoryModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() =>
      'CategoryModel(id: $id, name: $name, isSelected: $isSelected)';

  @override
  bool operator ==(covariant CategoryModel other) {
    if (identical(this, other)) return true;

    return other.id == id &&
        other.name == name &&
        other.isSelected == isSelected;
  }

  @override
  int get hashCode => id.hashCode ^ name.hashCode ^ isSelected.hashCode;
}

class LocationModel {
  final String name;
  final double latitude;
  final double longitude;
  final double? distance;
  LocationModel({
    this.name = '',
    this.latitude = 0.0,
    this.longitude = 0.0,
    this.distance,
  });

  LocationModel copyWith({
    String? name,
    double? latitude,
    double? longitude,
    double? distance,
  }) {
    return LocationModel(
      name: name ?? this.name,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      distance: distance ?? this.distance,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'latitude': latitude,
      'longitude': longitude,
      'distance': distance,
    };
  }

  factory LocationModel.fromMap(Map<String, dynamic> map) {
    return LocationModel(
      name: map['name'] as String,
      latitude: map['latitude'] as double,
      longitude: map['longitude'] as double,
      distance: map['distance'] != null ? map['distance'] as double : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory LocationModel.fromJson(String source) =>
      LocationModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'LocationModel(name: $name, latitude: $latitude, longitude: $longitude, distance: $distance)';
  }

  @override
  bool operator ==(covariant LocationModel other) {
    if (identical(this, other)) return true;

    return other.name == name &&
        other.latitude == latitude &&
        other.longitude == longitude &&
        other.distance == distance;
  }

  @override
  int get hashCode {
    return name.hashCode ^
        latitude.hashCode ^
        longitude.hashCode ^
        distance.hashCode;
  }
}
