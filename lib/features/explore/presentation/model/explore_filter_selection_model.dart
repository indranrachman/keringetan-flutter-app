import 'dart:convert';

// ignore_for_file: public_member_api_docs, sort_constructors_first
class ExploreFilterSelectionModel {
  String? selectedEventState;
  String? selectedCategory;
  ExploreFilterSelectionModel({
    this.selectedEventState,
    this.selectedCategory,
  });

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'selectedEventState': selectedEventState,
      'selectedCategory': selectedCategory,
    };
  }

  factory ExploreFilterSelectionModel.fromMap(Map<String, dynamic> map) {
    return ExploreFilterSelectionModel(
      selectedEventState: map['selectedEventState'] != null
          ? map['selectedEventState'] as String
          : null,
      selectedCategory: map['selectedCategory'] != null
          ? map['selectedCategory'] as String
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ExploreFilterSelectionModel.fromJson(String source) =>
      ExploreFilterSelectionModel.fromMap(
          json.decode(source) as Map<String, dynamic>);
}
