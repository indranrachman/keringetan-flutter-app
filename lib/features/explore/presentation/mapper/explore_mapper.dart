import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';
import 'package:keringetan_consumer_flutter_app/core/apis/event/model/response/event_response.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/date_time_utils.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';

abstract class ExploreMapper {
  List<EventModel> getExploreModel(List<EventResponse> responses);
}

@Injectable(as: ExploreMapper)
class ExploreMapperImpl implements ExploreMapper {
  @override
  List<EventModel> getExploreModel(List<EventResponse> responses) {
    return responses.map((response) => toEventModel(response)).toList();
  }

  EventModel toEventModel(EventResponse response) {
    final timeFormat = DateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    final dateTime = timeFormat.parse(response.date);
    final timeStamp = timeFormat.parse(response.createdAt);
    return EventModel(
      id: response.id,
      category: toCategoryModel(response),
      date: dateTime.format("EEE, dd MMM yyyy"),
      time: dateTime.format("HH:mm"),
      timeStamp: timeStamp.toTimeAgoLabel(),
      location: LocationModel(
        name: response.location.name,
        latitude: response.location.latitude,
        longitude: response.location.longitude,
      ),
      meetingNotes: response.meetingNotes,
      caption: response.caption,
      state: response.state,
      poster: toPosterModel(response),
    );
  }

  CategoryModel toCategoryModel(EventResponse response) {
    return CategoryModel(
      id: response.category.id,
      name: response.category.name,
      isSelected: false,
    );
  }

  UserModel toPosterModel(EventResponse response) {
    return UserModel(
      avatar: response.poster.avatar,
      id: response.poster.id,
      name: response.poster.name,
    );
  }
}
