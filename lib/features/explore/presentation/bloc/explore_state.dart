// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:injectable/injectable.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/model/explore_model.dart';

// ignore: must_be_immutable
@injectable
class ExploreState extends Equatable {
  final String? error;
  final bool? showLoading;
  final bool? pullToRefresh;
  final List<EventModel>? items;
  const ExploreState({
    this.error,
    this.showLoading,
    this.pullToRefresh,
    this.items,
  });

  ExploreState copyWith({
    String? error,
    bool? showLoading,
    bool? pullToRefresh,
    List<EventModel>? items,
  }) {
    return ExploreState(
      error: error,
      showLoading: showLoading,
      pullToRefresh: pullToRefresh,
      items: items,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'error': error,
      'showLoading': showLoading,
      'pullToRefresh': pullToRefresh,
      'items': items?.map((x) => x.toMap()).toList(),
    };
  }

  factory ExploreState.fromMap(Map<String, dynamic> map) {
    return ExploreState(
      error: map['error'] != null ? map['error'] as String : null,
      showLoading:
          map['showLoading'] != null ? map['showLoading'] as bool : null,
      pullToRefresh:
          map['pullToRefresh'] != null ? map['pullToRefresh'] as bool : null,
      items: map['items'] != null
          ? List<EventModel>.from(
              (map['items'] as List).map<EventModel?>(
                (x) => EventModel.fromMap(x as Map<String, dynamic>),
              ),
            )
          : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory ExploreState.fromJson(String source) =>
      ExploreState.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  bool get stringify => true;

  @override
  List<Object?> get props => [error, showLoading, pullToRefresh, items];
}
