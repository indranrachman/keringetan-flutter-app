// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'explore_bloc.dart';

class ExploreEvent extends Equatable {
  const ExploreEvent();

  @override
  List<Object> get props => [];
}

class ExploreFetchListEvent extends ExploreEvent {
  final String? state;
  final int? categoryId;
  const ExploreFetchListEvent({
    this.state,
    this.categoryId,
  });
}

class ExploreRefetchListEvent extends ExploreEvent {}

class ExploreShowEventDetailEvent extends ExploreEvent {}
