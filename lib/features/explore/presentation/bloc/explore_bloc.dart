// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import 'package:keringetan_consumer_flutter_app/core/apis/event/model/request/get_events_request.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/pref_utils.dart';
import 'package:keringetan_consumer_flutter_app/core/utils/user_location_service.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/data/explore_repository.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/bloc/explore_state.dart';
import 'package:keringetan_consumer_flutter_app/features/explore/presentation/mapper/explore_mapper.dart';

part 'explore_event.dart';

@injectable
class ExploreBloc extends Bloc<ExploreEvent, ExploreState> {
  final ExploreRepository repository;
  final ExploreMapper mapper;
  final UserLocationService locationService;
  final PrefUtils prefUtils;
  int limit = 10;
  int offset = 0;
  int? categoryId;
  String? eventState;

  ExploreBloc(
    this.repository,
    this.mapper,
    this.locationService,
    this.prefUtils,
  ) : super(const ExploreState()) {
    on<ExploreFetchListEvent>(_fetchExploreList);
    on<ExploreRefetchListEvent>(_refetchExploreList);
  }

  _fetchExploreList(
    ExploreFetchListEvent event,
    Emitter<ExploreState> emit,
  ) async {
    categoryId = event.categoryId;
    eventState = event.state;
    emit(state.copyWith(showLoading: true));
    await locationService
        .requestUserLocation()
        .then((value) => repository.getExploreList(
              GetEventsRequest(
                posterId: prefUtils.getUserId(),
                limit: limit,
                offset: offset,
                categoryId: categoryId,
                state: eventState,
              ),
            ))
        .then(
          (value) => emit(state.copyWith(
            showLoading: false,
            items: mapper.getExploreModel(value),
          )),
        )
        .catchError(
      (e, stacktrace) {
        emit(state.copyWith(error: e.toString()));
      },
    );
  }

  _refetchExploreList(
    ExploreRefetchListEvent event,
    Emitter<ExploreState> emit,
  ) async {
    emit(
      state.copyWith(pullToRefresh: true),
    );
    var response = await repository.getExploreList(
      GetEventsRequest(
        posterId: prefUtils.getUserId(),
        limit: limit,
        offset: offset,
        categoryId: categoryId,
        state: eventState,
      ),
    );
    emit(
      state.copyWith(
        pullToRefresh: false,
        items: mapper.getExploreModel(response),
      ),
    );
  }
}
